"""
Purpose     : Configuration and registration forms

| Revision History:
| Name:version 0.1
| Date: 02/2020
| Description: deployed

"""

from flask_wtf import FlaskForm as Form
from wtforms import ValidationError
import os
from shutil import copyfile
import yaml
from local_entero.routes.routes_utility_methods import compare_encrypt_text



from wtforms import StringField, PasswordField, IntegerField
from wtforms.validators import DataRequired, url
from wtforms.widgets import TextArea
import json
import requests


class Register_client_Form(Form):
    """
    A client registration form to enable the local administrator to  register his local instance ith Warwick EnteroBase.

    """
    Local_Name = StringField(label="Name* ", validators=[DataRequired()], render_kw={"placeholder": "You local EnteroBase instance name, required field"})
    Local_URL = StringField(label="URL*", validators=[DataRequired(), url()], render_kw={"cols": "32", "placeholder": "Your local EnteroBase instance  URL, required field"})
    Local_Description = StringField(label="Description*", widget=TextArea(), render_kw={"placeholder": "You local EnteroBase instance description (optional)"})

    def validate_Local_Name(self, field):
        check_client_attributes({'client_name':field.data})
        res = check_client_attributes({'client_name': field.data})
        if res.get('client_name') != 'OK':
            raise ValidationError(res.get('client_name'))

    def validate_Local_URL(self, field):
        res=check_client_attributes({'client_uri':field.data})
        if res.get('client_uri') !='OK':
            raise ValidationError(res.get('client_uri'))
        if not check_server_is_up(field.data):
            raise ValidationError("Server is down")


def check_client_attributes(data):
    try:
        from config import app_config
        enterobase_register_address = app_config.CHECK_CLIENT_REGISTER
        params_ = json.dumps(data)
        response = requests.get(enterobase_register_address+"?data="+params_, verify=False)
        return json.loads(response._content)
    except Exception as e:
        return {}

def check_server_is_up(uri):
    """
    check if the server is live and secure port 443 is open

    :param uri:

    """
    #the next line needs to be deleted when deploy the app in the production server, i.e. create the singularity container
    return True
    try:
        import socket
        if 'https://' in uri:
            uri=uri.replace('https://', '').strip()
        if 'http://' in uri:
            uri=uri.replace('http://', '').strip()
        if uri.endswith('/'):
            uri=uri[:-1]

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex((uri, 443))
        if result == 0:
            print ("Port is open")
            return True
        else:
            print("Port is not open")
            return False
    except :
        print ("Error")
        return False


class LogForm(Form):
    Application_Password= PasswordField(label="Administrator Password", validators=[DataRequired()], render_kw={"placeholder": "Please enter system password, required field"})
    next_url= StringField(label='',widget=TextArea(), render_kw={"style":"display: none;"})

    def validate_Application_Password(form, field):
        password = (field.data)
        if not compare_encrypt_text(password):
        #if app_config.app_password!=password:
            raise ValidationError("Password is not correct")


class ConfigurationForm(Form):
    """A form for enable the local administrator to configuration the local instance
    """

    def validate_ALLOWED_PROCESS(form, field):
        try:
            ss=(int(field.data))
        except:
            raise ValidationError("Value needs to be an integer")

    def validate_DATABASE_PORT(form, field):
        try:
            ss = (int(field.data))
        except:
            raise ValidationError("Value needs to be an integer")

def check_intege_value(value):
    if isinstance(value, int):
        return True
    return False

def create_configuration_form():
    """
    Read the yml file which contains the application configuration and
    create a form dynamically which allow the local administrator to modify it
    The configuration file name is '.local_configuration_file.yml'
    It is saved at the user home folder

    """
    LOCAL_INSTANCE_CONFIG = os.path.join(os.path.expanduser('~'), '.local_configuration_file.yml')
    not_displayed_list=['HAS_BEEN_TESTED','SECRET_KEY','CELERY_BROKER_URL','CELERY_RESULT_BACKEND','CSRF_SESSION_KEY']
    if not os.path.isfile(LOCAL_INSTANCE_CONFIG):
        from config import app_config
        copyfile( app_config.LOCAL_CONFIG_FILE , LOCAL_INSTANCE_CONFIG)
    with open(LOCAL_INSTANCE_CONFIG) as f:
        configs_ = yaml.load(f)
    configs = {}
    for key in sorted(configs_.keys(), reverse=True):
        configs[key] = configs_[key]

    for key, item in configs.items():
        if key in not_displayed_list:
            continue
        elif key.lower()== 'csrf_session_key':
            continue
        elif key=='BASE_FOLDER':
            label = "Working Directory"
        elif key=='CLIENT_SECRET':
            label = "Warwick client password"
        elif key=='CLIENT_TOKEN':
            label="Warwick temporary token"

        elif key=='CLIENT_ID':#CLIENT_ID':
            label = "Warwick client id"

        else:
            label = key.replace('_', ' ').capitalize()

        if isinstance( item, int):
            filed =IntegerField(label,  validators=[DataRequired()])
        else:
            filed=StringField(label,   validators=[DataRequired()])
        setattr(ConfigurationForm, key, filed)

    config_form=ConfigurationForm(data=configs)
    return config_form

