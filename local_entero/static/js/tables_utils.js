 var dataView;
  var slickgrid;
  var gridMenu;



var options = {
    enableCellNavigation: true,
    enableColumnReorder: false,
    multiColumnSort: true,
    explicitInitialization: true,
    showHeaderRow: true,
    headerRowHeight: 30,
    forceFitColumns: true,
    alwaysShowVerticalScroll: true, // this is necessary when using Grid Menu with a small dataset
    rowHeight: 30,
     // gridMenu Object is optional
    // when not passed, it will act as a regular Column Picker (with default Grid Menu image of drag-handle.png)
    gridMenu: {
      menuUsabilityOverride: function (args) {
        // we could disable the menu entirely by returning false
        return true;
      },
      customTitle: "Custom Menus",
      columnTitle: "Columns",
      hideForceFitButton: false,
      hideSyncResizeButton: false,
      // iconImage: "../images/drag-handle.png", // this is the Grid Menu icon (hamburger icon)
      // iconCssClass: "fa fa-bars",    // you can provide iconImage OR iconCssClass
      leaveOpen: false,                 // do we want to leave the Grid Menu open after a command execution? (false by default)
      // menuWidth: 18,                 // width that will be use to resize the column header container (18 by default)
      resizeOnShowHeaderRow: true,
      customItems: [
        {
          iconImage: "../images/delete.png",
          title: "Clear Filters",
          disabled: false,
          command: "clear-filter",
          cssClass: 'bold',     // container css class
          textCssClass: 'red'   // just the text css class
        },
        {
          iconImage: "../images/info.gif",
          title: "Toggle Filter Row",
          disabled: false,
          command: "toggle-filter",
          itemUsabilityOverride: function (args) {
            // for example disable the toggle of the filter bar when there's filters provided
            return isObjectEmpty(columnFilters);
          },
        },
        {
          iconImage: "../images/info.gif",
          title: "Toggle Top Panel",
          disabled: false,
          command: "toggle-toppanel",
          cssClass: 'italic',     // container css class
          textCssClass: 'orange'  // just the text css class
        },
        // you can pass divider as a string or an object with a boolean
        // "divider",
        { divider: true },
        {
          iconCssClass: "icon-help",
          title: "Help",
          command: "help",
          textCssClass: "blue",
          // you could dynamically remove a command from the list (only checks before opening the menu)
          // for example don't show the "Help" button if we have less than 5 columns left
          itemVisibilityOverride: function (args) {
            return args.visibleColumns.length > 4;
          },
          action: function(e, args) {
            // you can use the "action" callback and/or subscribe to the "onCallback" event, they both have the same arguments
            console.log('execute an action on Help', args);
          }
        },
        {
          iconImage: "",
          title: "Disabled Command",
          disabled: true,
          command: "custom-command"
        }
      ]
    }
  };
  var columns = [{ id: 'id', field: 'id', name: '#', width: 40, excludeFromGridMenu: true }];
  var columnFilters = {};
  for (var i = 0; i < 10; i++) {
    columns.push({
      id: i,
      name: String.fromCharCode("A".charCodeAt(0) + i),
      field: i,
      width: 60
    });
      };



function dayFormatter(row, cell, value, columnDef, dataContext) {
      return value + ' days';
  }
function dateFormatter(row, cell, value, columnDef, dataContext) {
      return value.getMonth() + '/' + value.getDate() + '/' + value.getFullYear();
  }

function formatter(row, cell, value, columnDef, dataContext) {
      return value;
  }

function set_formatter() {
    for (i in columns){
    if ('datatype' in columns[i])
        {
            if (columns[i]['datatype']=='text')
                columns[i]["formater"]=formatter;
            else if (columns[i]['datatype']=='date')
            {
                columns[i]["formater"]=dateFormatter;
            }
        }
    }
}

function filter(item) {
    for (var columnId in columnFilters) {
    console.log("col filter id: ", columnId);
      if (columnId !== undefined && columnFilters[columnId] !== "") {
        var c = slickgrid.getColumns()[slickgrid.getColumnIndex(columnId)];
        console.log("C: ",item[c.field],", Value: " , columnFilters[columnId]);
        if (!item[c.field].toLowerCase().includes(columnFilters[columnId].toLowerCase())){
        //if (item[c.field] != columnFilters[columnId]) {
          return false;
        }
      }
    }
    return true;
  }


function toggleGridMenu(e) {
    gridMenuControl.showGridMenu(e);
  }


function syncGridSelection(slickgrid, preserveHidden) {
  var self = this;
  var selectedRowIds = self.mapRowsToIds(slickgrid.getSelectedRows());;
  var inHandler;

  function update() {
    if (selectedRowIds.length > 0) {
      inHandler = true;
      var selectedRows = self.mapIdsToRows(selectedRowIds);
      if (!preserveHidden) {
        selectedRowIds = self.mapRowsToIds(selectedRows);
      }
      slickgrid.setSelectedRows(selectedRows);
      inHandler = false;
    }
  }

  slickgrid.onSelectedRowsChanged.subscribe(function(e, args) {
    if (inHandler) { return; }
    selectedRowIds = self.mapRowsToIds(slickgrid.getSelectedRows());
  });

  this.onRowsChanged.subscribe(update);

  this.onRowCountChanged.subscribe(update);
}

function syncGridCellCssStyles(slickgrid, key) {
  var hashById;
  var inHandler;

  // since this method can be called after the cell styles have been set,
  // get the existing ones right away
  storeCellCssStyles(slickgrid.getCellCssStyles(key));

  function storeCellCssStyles(hash) {
    hashById = {};
    for (var row in hash) {
      var id = rows[row][idProperty];
      hashById[id] = hash[row];
    }
  }

  function update() {
    if (hashById) {
      inHandler = true;
      ensureRowsByIdCache();
      var newHash = {};
      for (var id in hashById) {
        var row = rowsById[id];
        if (row != undefined) {
          newHash[row] = hashById[id];
        }
      }
      slickgrid.setCellCssStyles(key, newHash);
      inHandler = false;
    }
  }

  slickgrid.onCellCssStylesChanged.subscribe(function(e, args) {
    if (inHandler) { return; }
    if (key != args.key) { return; }
    if (args.hash) {
      storeCellCssStyles(args.hash);
    }
  });

  this.onRowsChanged.subscribe(update);

  this.onRowCountChanged.subscribe(update);
}

//////////////


function set_status (e, args) {

      if (slickgrid.getColumns()[args.cell].id !== 'status') return null;

      if (args.value == null || args.value === "") {
        return null;
      } else if (args.value=="Failed"||args.value=="FAILURE") {
        return "red";
      } else if (args.value =="Uploaded") {
        return "yellow";
      } else if (args.value =="Running" ||args.value =="Processing") {
        return "orange";
      }
      else {
        return "green";
      }
      return null;
    }

/////////////

function set_table(container)
{
const groupItemMetadataProvider = new Slick.Data.GroupItemMetadataProvider();


  set_formatter();

  //const
  dataView = new Slick.Data.DataView({
    groupItemMetadataProvider: groupItemMetadataProvider,
    inlineFilters: true
});

  //var dataView = new Slick.Data.DataView({ inlineFilters: true });

    slickgrid = new Slick.Grid($('#'+container), dataView, columns, options);
     var pager = new Slick.Controls.Pager(dataView, slickgrid, $("#pager"));

     gridMenuControl = new Slick.Controls.GridMenu(columns, slickgrid, options);


    dataView.onRowCountChanged.subscribe(function (e, args) {
          slickgrid.updateRowCount();
          slickgrid.render();
        });

    dataView.onRowsChanged.subscribe(function (e, args) {
          slickgrid.invalidateRows(args.rows);
          slickgrid.render();
        });

 $(slickgrid.getHeaderRow()).on("change keyup", ":input", function (e) {
      var columnId = $(this).data("columnId");
      if (columnId != null) {
        columnFilters[columnId] = $.trim($(this).val());
        dataView.refresh();
      }
    });
    slickgrid.onHeaderRowCellRendered.subscribe(function(e, args) {
        $(args.node).empty();
        $("<input type='text'>")
           .data("columnId", args.column.id)
           .val(columnFilters[args.column.id])
           .appendTo(args.node);
    });

    dataView.beginUpdate();

dataView.setItems(data);
    dataView.setFilter(filter);
    dataView.endUpdate();

slickgrid.onSort.subscribe(function (e, args) {
      var cols = args.sortCols;
      var data_=dataView.getItems();
      data_.sort(function (dataRow1, dataRow2) {
        for (var i = 0, l = cols.length; i < l; i++) {
          var field = cols[i].sortCol.field;
          var sign = cols[i].sortAsc ? 1 : -1;
          var value1 = dataRow1[field], value2 = dataRow2[field];
          var result = (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
          if (result != 0) {
            return result;
          }
        }
        return 0;
      });
      dataView.setItems(data_);
      slickgrid.invalidate();
      slickgrid.render();
    });

     slickgrid.onBeforeAppendCell.subscribe(set_status);
    // subscribe to Grid Menu event(s)
    gridMenuControl.onCommand.subscribe(function(e, args) {
      if(args.command === "toggle-filter") {
        slickgrid.setHeaderRowVisibility(!slickgrid.getOptions().showHeaderRow);
      }
      else if(args.command === "toggle-toppanel") {
        slickgrid.setTopPanelVisibility(!slickgrid.getOptions().showTopPanel);
      }
      else if(args.command === "clear-filter") {
        $('.slick-headerrow-column').children().val('');
        for (var columnId in columnFilters) {
          columnFilters[columnId] = "";
        }
        dataView.refresh();
      } else {
        alert("Command: " + args.command);
      }
    });
    // subscribe to event when column visibility is changed via the menu
    gridMenuControl.onColumnsChanged.subscribe(function(e, args) {
      console.log('Columns changed via the menu');
    });
    // subscribe to event when menu is closing
    gridMenuControl.onMenuClose.subscribe(function(e, args) {
      console.log('Menu is closing');
      slickgrid.autosizeColumns();
    });
    slickgrid.onAutosizeColumns.subscribe(function(e, args) {
      console.log('onAutosize called')
    });
    slickgrid.init();


}