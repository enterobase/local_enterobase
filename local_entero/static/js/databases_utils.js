var run_it=true;




//display message to the user
function displayMessage(header, body, btn_text)
{
messageHeader.innerText = header;
  messageBody.innerText =body;
if ( typeof(btn_text) !== "undefined" && btn_text !== null )
       moelButton.innerText =btn_text;
  $("#modal").modal();
}


function read_file_basic_check(source)
{
    var filename = source.files[0].name;
    //check file name for sepcial characters
   restricted_chrs=['-','!','@','#','$','%','^','&','*','(',')','+','=','[',']','{','}',';','\'',':','"','|',',','<','>','/','?'];
      for (cr in restricted_chrs)
       {
       if (filename.indexOf(restricted_chrs[cr]) > -1)
           {
              header = "Upload read file error";
              body="The file name should not contain special characters like [-!@#$%^&*()+=[]{};':\"|,<>/?]"
              displayMessage(header, body);
              source.value='';
              return;
           }
       }

//check if file has been used before
    if (user_files_list.indexOf(filename)>-1)
     {
          header = "Upload read file error";
          body=filename+" is associated with other strain";
          displayMessage(header, body);
          source.value='';
          return;
      }
//check file name (extension should be gz)
      if (!filename.toLowerCase().endsWith(".gz"))
      {
          header = "Upload read file error";
          body=filename+" needs to be gzip file";
          displayMessage(header, body);
          source.value='';
          return ;
      }
//check file size (should be > 2MB)
    var file_size=source.files[0].size;

    if (file_size < 2097152)
    {
          header = "Upload read file error";
          body=filename+" ("+file_size+" byte) size is less than 2MB";
          displayMessage(header, body);
          source.value='';
          return;
    }

    return "Done";

}

function fileValideForm() {
    var source = event.target || event.srcElement;
    //var pieces = source.value.split('\\');
    res=read_file_basic_check(source);
    var filename = source.files[0].name;
    if (source.value)
    {
      var r1 = document.getElementById("Readfile1").value;
      var r2 = document.getElementById("Readfile2").value;
      if (r1 == r2)
      {
         header = "Upload read file error";
         body="read files need to be diffrent";
         displayMessage(header, body);
         source.value=''
      }
    }
}

// add client checks to the strain metadata submit form
function _get_read_files(tag, read_files){
$("[id$=tag]").each(function() {
  file_fake=$(this ).val();
  if (file_fake)
  {
  file_=file_fake.split("//");
  read_files.push(files_[file_.length-1]);
  }


});
}

function fileValideForm_2(event)
{
read_files=[];
var source = event.target || event.srcElement;
var filename = source.files[0].name;
//alert("Hi file: "+filename);

//file_name=source.target.files[0].name;
res=read_file_basic_check(source);


if (source.value)
{

//C:\fakepath\2-Ec201510609_S2_R2.fastq.gz
//C:\fakepath\_Ec201808622_S-211_R2.fastq.gz
$("[id$='Readfile1']").each(function() {
  file_fake=$(this ).val();

  if (file_fake)
  {
  files_=file_fake.split("\\");
  //if (filename!=files_[files_.length-1])
    read_files.push(files_[files_.length-1]);
  }


});
$("[id$='Readfile2']").each(function() {
  file_fake=$(this ).val();
   if (file_fake)
  {
  files_=file_fake.split("\\");

  //if (filename!=files_[files_.length-1])
   read_files.push(files_[files_.length-1]);
  }

});

var no=0;
var i;
 for (i = 0; i < read_files.length; i++)
    {
     if (read_files[i]==filename)
    no=no+1;
    }

if (no>1)
{
header = "Upload read file error";

  body="Read file: "+filename +" has been used before";
  displayMessage(header, body);

  source.value=''

}


  //alert(read_files.length+read_files[0]+", Number: "+no);

}
}
function set_default()
{

if (strainMetadataGridTableForm)
  {
 var Readfile1 = $("[id$='Readfile1']");
 //alert(Readfile1);
$("[id$='Readfile1']").each(function() {
  $(this )
  .on('change', function(e) {
    fileValideForm_2(e);
});

});

$("[id$='Readfile2']").each(function() {
  $( this ).on('change', function(e) {
    fileValideForm_2(e);
});


});


  //alert ("I am in ");
  $("#inputID")
  "strains_table";
  var nodes = document.getElementById("strains_table").getElementsByTagName('*');
for(var i = 0; i < nodes.length; i++){

}
}


  else
  {
try
{
deafult_cont="Europe";
document.getElementById("Location-0-continent").value = deafult_cont;
//document.getElementById("Location-0-continent").value = "United Kingdom";
var cntr=document.getElementById("Location-0-country")
 cntr.options.length=0;
        ///
        for (var i = 0; i < continents_countries["Europe"].length; i++) {
            var option = document.createElement("option");
            option.value = continents_countries[deafult_cont][i];
            option.text = continents_countries[deafult_cont][i];
            cntr.appendChild(option);
           }
           ///
  cntr.value = "United Kingdom";
  }
  catch(error) {
  }
}
send_form();
}

var ajaxCall;

function cancell_function()
{


ajaxCall.onreadystatechange = null;
ajaxCall.abort();

 console.log("Canceled");
     $('#upload_btn').show();
    //$('#messages').hide();
     ajaxCall=null;


 return;
}


function get_files_from_from(form_name){
elements=document.getElementById(form_name).elements;
var files_list=[];
for (var i = 0, element; element = elements[i++];) {
    if (element.type === "file" )
    {
    console.log("elment", element.files[0]);
    files_list.push(element);
    }

    console.log("element type", element.type);



}
return files_list;

}

function progress(e){

    if(e.lengthComputable){
    console.log("====>>>>>", typeof e, Object.getOwnPropertyNames(e));
        var max = e.total;
        var current = e.loaded;

        var Percentage = (current * 100)/max;
        console.log(parseFloat(Percentage).toFixed( 2 ));
        $('#progBar').val(Percentage);

        //$('#upload_progress_bar').css('width', parseFloat(Percentage).toFixed( 2 ));
       // $('#upload_progress_bar').css('aria-valuenow', parseFloat(Percentage).toFixed( 2 ));

//$( "#upload_progress_bar" ).progressbar({
//      value: Percentage
  //  });
 //$("#upload_progress_bar").progressbar("value", Percentage);
        if(Percentage >= 100)
        {
           // process completed
                       $('#messages').hide();

        }
    }
 }
function goSleep(seconds) {
var e = new Date().getTime() + (seconds * 1000);
  while (new Date().getTime() <= e) {}

}

function send_form()
{
   $('#files_form').hide();
    console.log("Sedning the data");

    console.log(strain_url);
   $('#meta_form').submit(function (e) {
    e.preventDefault(); // block the traditional submission of the form.
    form=$(this)

   console.log("sending the form");
   $.ajax({
            type: "POST",
            url: strain_url,
            data: form.serialize(), // serializes the form's elements.
            dataType:'json',
            success: function (data) {
            if ('error' in data)
            {
            alert("form has error: "+data['error']);
            return;
            }
                console.log("suc::",data) ; // display the returned data in the console.
                 $('#meta_form').hide();
                $('#title_metadata').hide();
                $('#files_form').show();
                $('#title_upload_files').show();
                upload_files();

            },
            error : function(request,error)
    {
        alert("Request: "+JSON.stringify(request));

    }

        });
    });
    console.log("set files");
    $('#files_form').submit(function (e) {
    e.preventDefault(); // block the traditional submission of the form.
    upload_files();
    });
    // Inject our CSRF token into our AJAX request.
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrf_token)
            }
        }
    });

 return;
}

failed=[];

function upload_files(){
    console.log("Sending the files ...")
    form=$(this);
    var myParent = $(this).parent();

    $('#upload_btn').hide();
    $('#messages').show();
    files_list=get_files_from_from('files_form');
    var ins = files_list.length;
    if(ins == 0) {
        $('#msg').html('<span style="color:red">Select at least one file</span>');
        return;
    }


   console.log("sending the files");
   console.log("");


  for (var x = 0; x < ins; x++)
  {
  proj = files_list[x].files[0].name.split(".").join("");
  if ($('#'+proj).val()==100)
  continue;
  //proj=Number(files_list[x].files[0].name);
                  $('#files_form').show();
                 // $('#upload_files_txt').text("Please wait, uploading read file:  "+files_list[x].files[0].name);
            //var form_data = new FormData(files_list[0]);
      var form_data = new FormData();
      //console.log(files_list[x]);
      form_data.append("files[]", files_list[x].files[0]);

  ajaxCall=$.ajax({
  url:'upload_files',

  dataType: 'json', // what to expect back from server
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					async: true,
					xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                 if(myXhr.upload){
      myXhr.upload.addEventListener('progress', function(e){

    if(e.lengthComputable){
    console.log("====>>>>>", typeof e, Object.getOwnPropertyNames(e));
        var max = e.total;
        var current = e.loaded;

        var Percentage = (current * 100)/max;

// Assuming we have an empty <div id="container"></div> in
// HTML
var bar = new ProgressBar.Line('#files_form', {easing: 'easeInOut'});
//bar.animate(1);  // Value from 0.0 to 1.0



        console.log(proj,"=>>>>>>>>", parseFloat(Percentage).toFixed( 2 ), proj, typeof myParent.find('#'+proj), Object.getOwnPropertyNames(myParent.find('#'+proj).prevObject) );
        $('#'+proj).val(Percentage);
        $('#progBar').val(Percentage);

        //        bar.animate(Percentage/100);


        //$('#upload_progress_bar').css('width', parseFloat(Percentage).toFixed( 2 ));
       // $('#upload_progress_bar').css('aria-valuenow', parseFloat(Percentage).toFixed( 2 ));

//$( "#upload_progress_bar" ).progressbar({
//      value: Percentage
  //  });
 //$("#upload_progress_bar").progressbar("value", Percentage);
        if(Percentage >= 100)
        {
           // process completed
                       //$('#messages').hide();
                     //  alert($('#'+proj).val());

        }
    }
 });
                }
                return myXhr;
        },
					type: 'post',

            beforeSend: function()
            {

            },
                success: function (data) {
                    console.log("suc::",data) ; // display the returned data in the console.
                    //alert("suc: "+$('#'+proj).val());
                    upload_files();
            },
            error : function(request,error)
    {
        //alert("Request: "+JSON.stringify(request));
        console.log("Error ", error, JSON.stringify(request));
                       // alert("fail: "+$('#'+proj).val());
                        upload_files();
                        failed.push(proj);

    }

        });

                        // $('#files_form').hide();

        return;
     }
     if (failed.length==0)
     {
        window.location.replace(job_status);
     }
     else
        alert("Sorry, something went wrong ....");
    }


function continents_countriesValideForm()
{
    var source = event.target || event.srcElement;

    if (source.name=="Location-0-Continent"){//change in content
        var contry=document.getElementById("Location-0-Country").value
        //if (continents_countries[coutry]!=source.value)
        {
         var cntr=document.getElementById("Location-0-Country")
        cntr.options.length=0;
        ///
        for (var i = 0; i < continents_countries[source.value].length; i++) {
            var option = document.createElement("option");
            option.value = continents_countries[source.value][i];
            option.text = continents_countries[source.value][i];
            cntr.appendChild(option);
           }
        ////
        }

        }
    else///change in country
        {
        //var continent=document.getElementById("Location-0-Continent").value
        //if (continents_countries[source.value]!=continent)
        {
        //continent=document.getElementById("Location-0-Continent").value=continents_countries[source.value];
        }

    }
}




function validateMetaForm() {

//ToDo add additional check before submitting the for to the server
//check and return false if something is not correct
//and return the following line to prevent submittingthe form if something is wrong and
//return false;

var release_date= $('#release_date').val();

$('#meta_form').append('<input type="hidden" name="release_period" value='+release_date+'>');

//alert( "Hi there  "+release_date);
file_names=get_file_records();
for (var file_name in file_names) {
    // check if the property/key is defined in the object itself, not in parent
    if (file_names.hasOwnProperty(file_name))
    $('#meta_form').append('<input type="hidden" name="'+file_name+'" value='+file_names[file_name]+'>');
    }


return true;

//submit_the_form();

}

function submit_the_form()
{

//$("#strain_tabme").attr('disabled','disabled');
//("#strain_tabme").children().attr("disabled","disabled");
// This will disable all the children of the div
var nodes = document.getElementById("strain_tabme").getElementsByTagName('*');
for(var i = 0; i < nodes.length; i++){
//if (nodes[i].id=="submit_btn")
  nodes[i].setAttribute("hidden","")
//else
//     nodes[i].prop('disabled', true);
//nodes[i].setAttribute("readonly","true");//.disabled = true;
}
 $('#loading').show();
$("body").css("cursor", "progress");
}



function sub_group_Function(event )
{
   var target = event.target || event.srcElement;
    if (event.target.nodeName=="LABEL")
      target = target.parentNode;

for (i = 0; i < target.children.length; i++) {
  if (target.children.item(i).nodeName=="DIV")
  {
   target.children.item(i).style.visibility = 'visible';//'visible';//style.backgroundColor = "red";
   if (target.children.item(i).style.display=='block')
      target.children.item(i).style.display='none';
   else
      target.children.item(i).style.display='block';
   }
}

}

function set_not_visible(event)
{

   var target = event.target || event.srcElement;
    if (event.target.nodeName=="LABEL")
      target = target.parentNode;
    for (i = 0; i < target.children.length; i++) {
  if (target.children.item(i).nodeName=="DIV")
  {
    target.children.item(i).style.display='none';

   }
}
}

function set_visible(event)
{
   var target = event.target || event.srcElement;
    if (event.target.nodeName=="LABEL")
      target = target.parentNode;
    for (i = 0; i < target.children.length; i++) {
  if (target.children.item(i).nodeName=="DIV")
  {
    target.children.item(i).style.display='block';

   }
}
}


function _clean_the_form_(form_name)
{
    var to_be_deleted=[]

    elements=document.getElementById(form_name).elements;
    for (var i = 0, element; element = elements[i++];)
    {
        if (element.type === "file" )
        {
        to_be_deleted.push(element);
        }
    }
    for (var i=0; i<to_be_deleted.length;i++)
       {
       document.getElementById(form_name).removeChild(to_be_deleted[i]);
       }
}

function get_file_records()
{
    _clean_the_form_("files_form");
    file_names={}
    elements=document.getElementById("meta_form").elements;
    let files_form = document.getElementById("files_form");
    for (var i = 0, element; element = elements[i++];) {
        if (element.type === "file" )
        {
           console.log(element.name+":"+element.type);
           //f_child=element.cloneNode();
           //f_child.disabled = true;
    __child=element.cloneNode();

    file_names[__child.id]=__child.files[0].name;

    //__child.prop( "disabled", true );
    __child.setAttribute("disabled","disabled");
    $('#files_form').append(__child);
    //files_form.insertBefore(f_child, files_form.firstChild);
    var child = document.createElement('progress');
    child.setAttribute("id",element.files[0].name.split(".").join(""));



    child.setAttribute("value","0");
    child.setAttribute("max","100");
     $('#files_form').append(child);
    console.log("adding", element.files[0].name);
    var child_2 = document.createElement('div');
    child_2.innerHTML = "<br>";
    $('#files_form').append(child_2);

    }
    console.log("End..........");

    }

    return file_names;
 }




 function set_jobs_status_table(row, columns, container_name)
 {

 }
    /*
    if (target.children.item(i).nodeName=="DIV")
      {
        target.children.item(i).style.display='block';

       }


    var sForm = $('#meta_form');

    alert("HI Mthod ...");
    $.each(sForm[0].elements, function(index, elem){
        //Do .
        //alert(elem.nodeName);
        try
        {
        elem.clone().appendTo($('#files_form'));
        }
        catch(error) {
        alert(error);
      }
    });
    return;
    */



//onmouseover="set_visible(event)"