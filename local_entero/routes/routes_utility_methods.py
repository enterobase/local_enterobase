"""
Prurpose: contains the utility funtion which are used by routes methods. e.g create froms, upload files, etc

| Revision History:
| Name:version 0.1
| Date: 02/2020
| Description: deployed

"""


from werkzeug import secure_filename
import os
import local_entero.databases.databases_utilities as d_util
from local_entero import local_app, db, app_config
import datetime
import pandas as pd
import numpy as np
from flask import jsonify
import yaml
from shutil import copyfile
import secrets, hashlib
import json
import requests
from cryptography.fernet import Fernet


def encrypt_text(text):
    cipher_suite = Fernet(local_app.config.get('ENCRYPT_KEY'))

    return cipher_suite.encrypt(text.encode())

def get_password():
    e_password=local_app.config.get('APP_PASSWORD')
    return decrypt_text(e_password)


def compare_encrypt_text(input_text, encrypt_text=None):
    """
    It is used to compare input text with encrypted one
    if encrypted text is not provided it will compare using the encrypted application password.

    :param input_text:
    :param encrypt_text:


    """
    if not encrypt_text:
        if not local_app.config.get('APP_PASSWORD'):
            return False
        if get_password()==input_text:
            return True
        else:
            return False
    else:
        if input_text==decrypt_text(encrypt_text):
            return True
        else:
            return False


def decrypt_text(encrypt_text):
    cipher_suite = Fernet(local_app.config.get('ENCRYPT_KEY'))
    uncipher_text = (cipher_suite.decrypt(encrypt_text))
    return bytes(uncipher_text).decode("utf-8")


def get_database_title(databases, database):
    title=None
    for dbase in databases:
        try:
            if dbase['name'] == database:
                title = dbase['label']
                break
        except:
            pass

    return title

def _get_kyes_req(fields_req, lables_names, field):
    ignore_list=['Accession No.','Experiment','Sequencing Platform','Sequencing Library']
    if field.get('label') in ignore_list:
        return
    if field.get('required'):
        fields_req[field.get('label')] = field.get('required')

    lables_names[field.get('label')]=field.get('name')


def get_jobs_status_column():

    # This should be modified later to get the jobs status columns dynamically

    columns=[
        {
        "name": "Name",
        "field": "strain",
        "id": "strain",
        #"formatter":"Slick.Formatters.PercentCompleteBar",
        "sortable": "true",
        "datatype":"text"
        },
        {
            "name": "Uploaded Date",
            "field": "uploaded_date",
            "id": "uploaded_date",
         #   "formatter": "dateFormatter",
            "sortable": "true",
            "datatype": "date"
        },
        {
            "name": "Release Date",
            "field": "release_date",
            "id": "release_date",
          #  "formatter": "dateFormatter",
            "sortable": "true",
            "datatype": "date"
        }
        ,
        {
            "name": "Pipline",
            "field": "current_pipline",
            "id": "current_pipline",
            "sortable": "false",
            "datatype": "text"
        }
        ,
        {
            "name": "Status",
            "field": "status",
            "id": "status",
            "sortable": "true",
            "datatype": "text"
        }
        ,
        {
            "name": "Notes",
            "field": "notes",
            "id": "notes",
            "sortable": "true",
            "datatype": "text"
        }
        ,
        {
            "name": "Entero Barcode",
            "field": "entero_barcode",
            "id": "notes",
            "sortable": "true",
            "datatype": "text"
        }
        ]

    return columns


def get_strains_meta_data_from_file(data_form, user_id,strain_metadata_columns):
    data_file = secure_filename(data_form.DataFile.data.filename)
    user_folder = local_app.config.get('LOCAL_UPLOAD_DIR') + '/' + str(user_id)
    if not os.path.exists(user_folder):
        os.mkdir(user_folder)
    temp_folder = user_folder + r'/temp'
    if not os.path.exists(temp_folder):
        os.mkdir(temp_folder)

    data_f = temp_folder + '/' + data_file
    data_form.DataFile.data.save(data_f)
    meta_d=pd.read_csv(data_f, sep='\t', lineterminator='\r')
    linesList = [line.rstrip('\n') for line in open(data_f)]
    lines=[]
    lables_names={}
    for line in linesList:
        lines.append(line.split('\t'))
    fields_req={'Read Files':1}
    for field in strain_metadata_columns:
        if isinstance(field, list):
            for fie in field:
                _get_kyes_req(fields_req,lables_names, fie)
            #for it in field:
        else:
            _get_kyes_req(fields_req, lables_names,field)
    missed_req_values={}
    for index, row in meta_d.iterrows():
        m_val=[]
        for k in fields_req.keys():
            if k not in row or (type(row[k]) == float and np.isnan(row[k])):
                m_val.append(k)

            else:
                pass#print("key: ", k, row[k], type(row[k]))
            if len(m_val)>0:
                missed_req_values[index]=m_val

    meta_d_2 = meta_d.rename(columns=lables_names)  # old method
    md_json=meta_d.to_json(orient='records')
    return md_json, missed_req_values, fields_req.keys()

def update_read_files(reads_form,database, user_id, enterobase_id, st_id):
    modified_fields={'status': 'Uploaded'}
    read_1 = secure_filename(reads_form.Readfile1.data.filename)
    read_2 = secure_filename(reads_form.Readfile2.data.filename)
    user_folder = local_app.config.get('LOCAL_UPLOAD_DIR') + '/' + str(enterobase_id)
    if not os.path.exists(user_folder):
        os.mkdir(user_folder)
    database_folder = user_folder + '/' + database
    if not os.path.exists(database_folder):
        os.mkdir(database_folder)
    read_file_1_location = database_folder + '/' + read_1
    read_file_2_location = database_folder + '/' + read_2
    reads_form.Readfile1.data.save(read_file_1_location)
    reads_form.Readfile2.data.save(read_file_2_location)
    modified_fields["accession"] = read_file_1_location + ',' + read_file_2_location
    modified_fields[
        "client_Readfiles"] = reads_form.Readfile1.data.filename + ',' + reads_form.Readfile2.data.filename

    d_util.modify_raw_strain_(modified_fields, st_id, db)
    job_data = {}
    job_data['read_files'] = modified_fields.get('accession')
    job_data['user_id'] = user_id
    job_data['database'] = database
    job_data['pipeline'] = 'QAssembly'
    job_data['priority'] = 0
    job_data["strain_data_id"] = st_id
    job_data['status'] = 'Received'
    job_id=d_util.get_strains_data_job(st_id, db)
    d_util.update_job(job_id, job_data, db)
    return job_id, st_id

def add_raw_strain_data_jobs(strain_metadata_from, database, user_id, enterobase_id, release_period):
    """
    Extract the strains metadata and save the read files to user reads and
    insert it to the strain table and submit a job to jobs databases.

    :param strain_metadata_from:
    :param database:
    :param user_id:
    :param enterobase_id:

    """
    read_1 = secure_filename(strain_metadata_from.Readfile1.data.filename)
    read_2 = secure_filename(strain_metadata_from.Readfile2.data.filename)
    strain_metadata_from.Readfile2.data.filename = read_2
    local_upload_folder=local_app.config.get('LOCAL_UPLOAD_DIR')
    if not os.path.exists(local_upload_folder):
        #need to return error message to the user
        return "Please check the proovided user reads upload folder: %s"%local_upload_folder

    user_folder = local_app.config.get('LOCAL_UPLOAD_DIR') + '/' + str(enterobase_id)
    if not os.path.exists(user_folder):
        os.mkdir(user_folder)
    database_folder = user_folder + '/' + database
    if not os.path.exists(database_folder):
        os.mkdir(database_folder)
    read_file_1_location = database_folder + '/' + read_1
    read_file_2_location = database_folder + '/' + read_2
    strain_metadata_from.Readfile1.data.save(read_file_1_location)
    strain_metadata_from.Readfile2.data.save(read_file_2_location)
    strain_meta_data = strain_metadata_from.data
    strain_meta_data["accession"] = read_file_1_location + ',' + read_file_2_location
    strain_meta_data["client_Readfiles"] = strain_metadata_from.Readfile1.data.filename+','+ strain_metadata_from.Readfile2.data.filename
    strain_meta_data['collection_year']=strain_meta_data["collectiondate"].year
    strain_meta_data['collection_month'] = strain_meta_data["collectiondate"].month
    strain_meta_data['collection_date'] = strain_meta_data["collectiondate"].day
    strain_meta_data['collection_day'] = strain_meta_data["collectiondate"].day
    #strain_meta_data['']='assembled_by_client'
    strain_meta_data['release_period']=strain_meta_data
    strain_meta_data['seq_platform']='illumina'
    strain_meta_data['seq_library']='paired'

    to_delete=["Readfile1","Readfile2","csrf_token","collectiondate"]
    to_be_added={}
    for key, val in strain_meta_data.items():
        if isinstance(val, list):
            to_delete.append(key)
            for it in val:
                for k2, val2 in it.items():
                    to_be_added[k2]=val2

    for k, v in to_be_added.items():
        strain_meta_data[k]=v

    for key, val in strain_meta_data.items():
        if isinstance(val, list):
            continue
            del_temp=[]
            for k2, val2 in val.items():
                if not val2 or (isinstance(val2, str) and  val2.strip()==""):
                    del_temp.append(k2)
                    for k in del_temp:
                        del val[k]
            if len(val)==0:
                to_delete.append(key)
        if not val or (isinstance(val, str) and val.strip()) == "":
               to_delete.append(key)

    for k in to_delete:
        del strain_meta_data[k]
    st_id = d_util.add_raw_strain_(strain_meta_data, database, user_id, db)
    job_data = {}
    job_data['read_files'] = strain_meta_data.get('accession')
    job_data['user_id'] = user_id
    job_data['database'] = database
    job_data['pipeline'] = 'QAssembly'
    job_data['priority'] = 0
    job_data["strain_data_id"] = st_id
    job_id = d_util.add_job(job_data, db)
    return  job_id, st_id


###########################
def add_raw_strain_data_jobs_2(request, strain_metadata_from, database, user_id, enterobase_id):
    """
    get the strains metadata and save the read files to user reads and
    insert it to the strain table and submit a job to jobs databases.

    :param strain_metadata_from:
    :param database:
    :param user_id:
    :param enterobase_id:

    """
    release_period = request.form.get('release_period')
    read_file_1 = request.form.get("Readfile1")
    read_file_2 = request.form.get("Readfile2")
    read_1 = secure_filename(read_file_1)
    read_2 = secure_filename(read_file_2)
    #strain_metadata_from.Readfile2.data.filename = read_2
    user_folder = local_app.config.get('LOCAL_UPLOAD_DIR') + '/' + str(enterobase_id)
    if not os.path.exists(user_folder):
        os.mkdir(user_folder)
    database_folder = user_folder + '/' + database
    if not os.path.exists(database_folder):
        os.mkdir(database_folder)
    read_file_1_location = database_folder + '/' + read_1
    read_file_2_location = database_folder + '/' + read_2

    strain_meta_data = strain_metadata_from.data
    strain_meta_data["accession"] = read_file_1_location + ',' + read_file_2_location
    strain_meta_data[
        "client_Readfiles"] = read_file_1 + ',' + read_file_1
    strain_meta_data['collection_year'] = strain_meta_data["collectiondate"].year
    strain_meta_data['collection_month'] = strain_meta_data["collectiondate"].month
    strain_meta_data['collection_date'] = strain_meta_data["collectiondate"].day
    strain_meta_data['collection_day'] = strain_meta_data["collectiondate"].day
    # strain_meta_data['']='assembled_by_client'
    strain_meta_data['release_period'] = release_period
    strain_meta_data['seq_platform'] = 'illumina'
    strain_meta_data['seq_library'] = 'paired'

    to_delete = ["csrf_token", "collectiondate"]
    to_be_added = {}
    for key, val in strain_meta_data.items():
        if isinstance(val, list):
            to_delete.append(key)
            for it in val:
                for k2, val2 in it.items():
                    to_be_added[k2] = val2

    for k, v in to_be_added.items():
        strain_meta_data[k] = v

    for key, val in strain_meta_data.items():
        if isinstance(val, list):
            continue
            del_temp = []
            for k2, val2 in val.items():
                if not val2 or (isinstance(val2, str) and val2.strip() == ""):
                    del_temp.append(k2)
                    for k in del_temp:
                        del val[k]
            if len(val) == 0:
                to_delete.append(key)
        if not val or (isinstance(val, str) and val.strip()) == "":
            to_delete.append(key)

    for k in to_delete:
        del strain_meta_data[k]
    st_id = d_util.add_raw_strain_(strain_meta_data, database, user_id, db)
    data_r1={}
    data_r1["user_id"]=user_id
    data_r1["database"]=database
    data_r1["strain_data_id"]=st_id
    data_r1["file_name"]=read_file_1
    data_r1["file_location"] =read_file_1_location
    data_r1["status"] ="Waiting"

    data_r2 = {}
    data_r2["user_id"] = user_id
    data_r2["database"] = database
    data_r2["strain_data_id"] = st_id
    data_r2["file_name"] = read_file_2
    data_r2["file_location"] = read_file_2_location
    data_r2["status"] = "Waiting"
    d_util.add_read_files_records(data_r1, data_r2, db)

    return  st_id


def upload_file(files,database, enterobase_id):
    """
    :param files: files to be uploaded and saved in the user folder
    :param database:
    :param enterobase_id: enterobase user id

    """
    errors={}
    success = False
    res=None
    error_code=-1
    user_folder = local_app.config.get('LOCAL_UPLOAD_DIR') + '/' + str(enterobase_id)
    if not os.path.exists(user_folder):
        os.mkdir(user_folder)
    database_folder = user_folder + '/' + database
    if not os.path.exists(database_folder):
        os.mkdir(database_folder)
    for file in files:
        filename = secure_filename(file.filename)
        read_file_location = database_folder + '/' + filename
        try:
            file.save(read_file_location)
            res=update_check_strain_read_files_status(read_file_location)
            success = True
        except Exception as e:
            if file.filename== "S_A_R1.fastq.gz":
                error_code=400
            local_app.logger.error("upload_file error, message: %s"%e)
            errors[file.filename] = 'File is not uploaded'
            success = False
    if len(errors)>0 and not success:
        resp = jsonify(errors)
        if error_code==-1:
            resp.status_code=200
        else:
            resp.status_code =200
        local_app.logger.error("upload_file errorm messgae: %s"%resp)
    else:
        resp=jsonify({"message":"File succcefully uploaded"})
        resp.status_code=200
    return resp, res

def update_check_strain_read_files_status(file_location):
    st_id=d_util.update_read_file_status(file_location, "Uploaded", db)
    status= d_util.get_strain_read_files_status(st_id,db)
    for st in status:
        if st!="Uploaded":
            return
    # get the stains_data
    strain_data=d_util.get_raw_strain(st_id, db)
    # create the job
    job_data={}
    job_data['read_files'] = strain_data['data'].get('accession')
    job_data['user_id'] = strain_data.get("user_id")
    job_data['database'] = strain_data.get("database")
    job_data['pipeline'] = 'QAssembly'
    job_data['priority'] = 0
    job_data["strain_data_id"] = st_id
    job_id = d_util.add_job(job_data, db)
    return job_id

def get_user_jobs(user_id, database):
    user_jobs=d_util.get_user_jobs(db, database, user_id)
    user_strains_data = d_util.get_user_strains_data(db, database, user_id)
    st_ids={}
    for st in user_strains_data:
        st_ids[st.id]=st
    results=[]
    for job in user_jobs:
        if job.strain_data_id not in st_ids:
            continue
        row={"id":job.strain_data_id}
        row['database'] =database
        results.append(row)
        row['strain']=st_ids[job.strain_data_id].strain
        row['current_pipline']=job.pipeline
        row["selected"] = ""
        _date=st_ids[job.strain_data_id].date_uploaded.strftime("%b %d %Y %H:%M")
        row['uploaded_date']=_date

        if st_ids[job.strain_data_id].release_period and isinstance(st_ids[job.strain_data_id].release_period, int) :
            release_date=(datetime.date.today() + datetime.timedelta(st_ids[job.strain_data_id].release_period * 365 / 12)).isoformat()

            #strftime("%b %d %Y %H:%M")
            row['release_period'] = st_ids[job.strain_data_id].release_period
        else:
            row['release_period'] = 0
            release_date =st_ids[job.strain_data_id].date_uploaded.strftime("%b %d %Y %H:%M")
        row['status'] = st_ids[job.strain_data_id].status
        row['release_date']=release_date
        row['notes']='-'
        row['Entero_Barcode']='-'
    return results

def get_user_jobs_status(user_id, database):
    user_jobs=d_util.get_user_jobs(db, database, user_id)
    user_strains_data = d_util.get_user_strains_data(db, database, user_id)
    st_ids={}
    for st in user_strains_data:
        st_ids[st.id]=st

    results=[]
    for job in user_jobs:
        if job.strain_data_id not in st_ids:
            continue
        row={}
        results.append(row)
        row['id']=job.strain_data_id
        row['strain']=st_ids[job.strain_data_id].strain
        row['current_pipline']=job.pipeline
        _date=st_ids[job.strain_data_id].date_uploaded.strftime("%b %d %Y %H:%M")
        row['uploaded_date']=_date


        if st_ids[job.strain_data_id].release_period and isinstance(st_ids[job.strain_data_id].release_period, int) :
            release_date=(datetime.date.today() + datetime.timedelta(st_ids[job.strain_data_id].release_period * 365 / 12)).isoformat()

        else:
            release_date =st_ids[job.strain_data_id].date_uploaded.strftime("%b %d %Y %H:%M")
        row['status'] = st_ids[job.strain_data_id].status
        row['release_date']=release_date
        row['notes']='-'
        row['entero_barcode']='-'
    return results


def _prepare_the_data(enterobase_strain_metadata):
    ignore_list =["Sequencing Platform", "Sequencing Library", "Accession", "Experiment", "Bases", "Region", "District", "Post Code",
    # "Collection Time", "Collection Year", "Collection Month", "Collection Day",
                  "Barcode","Insert Size",
     "Date Entered", "Release Date", "Uberstrain", "Accession No.","Project ID","Bio Project ID", "Sample ID",
                  "Secondary Sample ID","Collection Month","Collection Day","Collection Time","Longitude","Latitude","City", "Continent", "Sample", "Secondary Sample", "Project"]

    prepared_metdata=[]
    to_be_added=[]
    for filed in enterobase_strain_metadata:
        if isinstance(filed, list):
            for filed_ in filed:
                if filed_.get('label') in ignore_list:
                    continue
                prepared_metdata.append(filed_)
                if filed_.get('label') != 'Collection Year':
                    filed_['columnGroup']=filed_.get('group_name')
                else:
                    filed_['label']='Collection Date'
                    filed_['name'] = 'collection_date'


        else:
            if filed.get('label') in ignore_list:
                continue

            prepared_metdata.append(filed)
    req_fields=[]
    for field in prepared_metdata:
        if field.get('required'):
            req_fields.append(field.get('name'))
    return prepared_metdata, req_fields


def _prepa_empty_data(grid_meteada):
    """
    create dataset which contains the default values.

    :param grid_meteada:

    """
    data=[]
    row = {}
    data.append(row)
    row['id'] = 0
    for col in grid_meteada:
        if col.get('default_value'):
            row[col.get('field')]=col.get('default_value')
        else:
            row[col.get('field')] =''
    row["country"]="France"
    return data


def _sort_metadata_columns(grid_meteada, req_fields ):
    """
    Sort column which will be sent to the front end to display required fields first.

    :param grid_meteada:
    :param req_fields:

    """
    sorted_grid=[]
    remain_grid=[]
    for item in grid_meteada:
        if item.get('field') in req_fields:
            sorted_grid.append((item))
        else:
            remain_grid.append(item)

    return sorted_grid+remain_grid


def get_grid_starins_metadata_columns(enterobase_strain_metadata):
    """
    Set table coulmn which will be sent to the front end to be displayed inside a table.

    :param enterobase_strain_metadata:

    """
    # get required fields.......How??
    #validator: requiredFieldValidator
    prepared_metdata, req_fields=_prepare_the_data(enterobase_strain_metadata)

    grid_meteada = []
    for st_metadata in prepared_metdata:
        filed_meteada={}
        grid_meteada.append(filed_meteada)
        for key, value in st_metadata.items():
            if value==None:
                value=''
            filed_meteada["sortable"]= "true"
            if key =="required" and value:
                filed_meteada["validator"]= "requiredFieldValidator"

            if key =='columnGroup' and value:
                filed_meteada['columnGroup'] = value

            if key=='label':
                filed_meteada['name']=value
            elif key== 'name':
                filed_meteada['field']=value
                filed_meteada['id'] = value
                filed_meteada["sortable"]: "true"
            elif key=="datatype":
                if value=='integer' and st_metadata.get('name')=='collection_date':
                    filed_meteada['name'] = "Collection date"
                    filed_meteada["datatype"] = "date"

                    filed_meteada["minWidth"] = 100
                    if filed_meteada.get('columnGroup'):
                        del filed_meteada['columnGroup']
                elif value == "text" and  st_metadata.get('name')=='continent':
                    vals = local_app.config.get('COUNTRIES')
                    filed_meteada["dataSource"] = vals
                    filed_meteada["datatype"] = "combo"
                    filed_meteada["editor"] = "Select2Editor"
                    filed_meteada["formatter"] = "Select2Formatter"
                    filed_meteada["minWidth"] = 100
                elif value== "combo":

                    if st_metadata.get('vals'):
                        vals = st_metadata.get('vals').split('|')
                        filed_meteada["dataSource"]=vals
                        filed_meteada["datatype"] = "combo"
                        filed_meteada["editor"]="Select2Editor"
                        filed_meteada["formatter"]= "Select2Formatter"
                        filed_meteada["minWidth"]=100

                elif value=="custom":
                    if st_metadata.get('name')=='country':
                        vals= local_app.config.get('COUNTRIES')
                        filed_meteada["dataSource"] = vals
                        filed_meteada["datatype"] = "combo"
                        filed_meteada["editor"] = "Select2Editor"
                        filed_meteada["formatter"] = "Select2Formatter"
                        filed_meteada["minWidth"] = 200

                    else:
                        #this need to be changed later to include the preferd data type
                        filed_meteada[key] = 'text'
                elif value=="suggest":
                    filed_meteada[key] = 'text'
                    filed_meteada["minWidth"] = 100
                else:
                    filed_meteada[key] = value
                    filed_meteada["minWidth"] = 160
            else:
                continue

    data=_prepa_empty_data(grid_meteada)
    file_item={"sortable": "false", "field": "read_files", "id":"read_files", "datatype":"file_editor", "minWidth":250, "name": "Read files"}
    grid_meteada=_sort_metadata_columns(grid_meteada,req_fields)
    req_fields.append(file_item.get("field"))
    grid_meteada.insert(1, file_item)
    return grid_meteada, data, req_fields


def get_strain_data_to_edit(id):
    res=d_util.get_raw_strain(id, db)
    return res


def get_form_validation_error(form):
    errors=''
    for item in form.errors.items():
        if not errors:
            errors = str(item) + "," + str(type(item))
        else:
            errors = errors + "\n" + str(item)+ "," + str(type(item))
    return errors

def _modify_config_file(filed, value):
    LOCAL_INSTANCE_CONFIG = os.path.join(os.path.expanduser('~'), '.local_configuration_file.yml')
    with open(LOCAL_INSTANCE_CONFIG) as f:
        configs = yaml.load(f)
    configs[filed]=value
    setattr(app_config,filed, value)
    local_app.config[filed]=value
    _write_config_file(LOCAL_INSTANCE_CONFIG, configs)

def _write_config_file(LOCAL_INSTANCE_CONFIG, data):
    # Copy old config file to .back file
    copyfile(LOCAL_INSTANCE_CONFIG, LOCAL_INSTANCE_CONFIG + ".back")
    # write the new config to the configuration file
    with open(LOCAL_INSTANCE_CONFIG, 'w') as outfile:
        yaml.dump(data, outfile, default_flow_style=False)

def check_config_changes(data):
    """
    check if the configuration has been changed,
    if so, the orginal file will be saved to .back file and
    the new configuration will be saved to the file

    :param data: the configuarion retuned from the adminstrator

    """
    LOCAL_INSTANCE_CONFIG = os.path.join(os.path.expanduser('~'), '.local_configuration_file.yml')
    is_it_changes=False
    change_required_restart=False
    not_required_change_list=['CLIENT_TOKEN']

    with open(LOCAL_INSTANCE_CONFIG) as f:
        configs = yaml.load(f)

    data['ALLOWED_PROCESS'] = int(data['ALLOWED_PROCESS'])
    data['DATABASE_PORT'] = int(data['DATABASE_PORT'])
    new_config={}
    for key, value in configs.items():
        new_config[key]=value
        if key=='HAS_BEEN_TESTED':
            continue
        if data.get(key) and data[key]!=value:
            new_config[key] = data[key]
            is_it_changes = True
            if key in not_required_change_list:
                local_app.config[key]=value
                setattr(app_config, key, value)
            else:
                change_required_restart=True
    if (is_it_changes):
        new_config['HAS_BEEN_TESTED'] = configs.get('HAS_BEEN_TESTED')
        _write_config_file(LOCAL_INSTANCE_CONFIG, new_config)
    return change_required_restart

class Struct:
    def __init__(self, **entries):
        self.__dict__.update(entries)

def _get_md5_file(filename):
    md5_hash = hashlib.md5()
    with open(filename, "rb") as f:
        for byte_block in iter(lambda: f.read(8192), b""):
            md5_hash.update(byte_block)
        return(md5_hash.hexdigest())

def _create_base_test_file():
    """
    check if the base file exist, if not create a new one

    """
    g_List=['A','T','C','G']
    base_test_file='test_fle.fasta'
    base_test_file = os.path.join(local_app.config.get('LOCAL_UPLOAD_DIR'), base_test_file)
    if not os.path.exists(base_test_file):
        lines = []
        for i in range(0, 50000):
            line = secrets.choice(g_List)
            for j in range(0, 100):
                line += secrets.choice(g_List)
            lines.append(line)
        contenets = '\n'.join(lines)
        file = open(base_test_file, "w")
        file.write(contenets)
        file.close()
    return base_test_file


def _creat_file_test_upload():
    """
     used to test the upload files to EnteroBase
     first it creates a file then it uploads it to Warwick EnteroBase
     it sends the file md5 and send it along with the file to EnteroBase.

    :return:True if the files is sent correctley and its caculate md5 ait Warwick EnteroBase is equal to the sent one
    """
    # First create a base file then make some changes for each file which is sent to Warwick EnteroBase
    g_List=['A','T','C','G']
    base_test_file = 'test_fle.fasta'
    base_test_file = os.path.join(local_app.config.get('LOCAL_UPLOAD_DIR'), base_test_file)
    f = open(base_test_file, 'r')
    base_contenets = f.readlines()
    f.close()
    line = secrets.choice(g_List)
    for j in range(0, 100):
        line += secrets.choice(g_List)
    base_contenets.append(line)
    r_name=secrets.token_urlsafe(8)+'.fasta'
    filename=os.path.join(local_app.config.get('LOCAL_UPLOAD_DIR'),r_name)
    contenets='\n'.join(base_contenets)
    file_ = open(filename, "w")
    file_.write(contenets)
    file_.close()
    md5=_get_md5_file(filename)
    start_time=datetime.datetime.now()
    enterobase_upload_test = local_app.config.get('TEST_UPLOADS_ADDRESS')
    token=local_app.config.get('CLIENT_TOKEN')
    data_={'token':token,"md5":md5}
    files = {'files': (r_name, open(filename, 'rb'), 'text/x-spam')}
    resp = requests.post(enterobase_upload_test, files=files,data=data_,  verify=False)
    res=resp.text
    end_time = datetime.datetime.now()
    elapsedTime=(end_time-start_time).total_seconds()
    os.remove(filename)
    return res, int(elapsedTime)

def _connect_Warwick_get_resp(url, data_):
    try:
        resp = requests.post(url, data=data_, verify=False)
        return resp.text
    except Exception as e:
        local_app.logger.error("Erro while connecting Warwick EnteroBase, error message: %s"%e)
        return ("Error")


def is_this_client_registered(token=None, BASE_URL_2=None):
    """
    Check if the client submitted a registration request by
    checking token in the app configuration with Warwick EnteroBase.

    """
    try:
        if not BASE_URL_2:
            BASE_URL_2=local_app.config.get("BASE_URL_2")
        url = BASE_URL_2 + '/oauth/is_registerd_request_submitted'
        if not token:
            token=local_app.config.get('CLIENT_TOKEN')
        data_={'token':token}
        results=_connect_Warwick_get_resp(url, data_)
    except Exception as e:
        local_app.logger.error ("errro in is_this_client_registered, error: %s"%e)
        results='False'
    return results

def submit_upload_test_results(no_test_uploaded_files, ave_upload_time):
    """
    sumbit test upload results to Warwick EnteroBase.

    :param no_files: no of uploaded files in th etest
    :param ave_upload_time: averrage upload time
    :return: the upload results
    """
    url=local_app.config.get("BASE_URL")+'/oauth/save_upload_test_results'
    token = local_app.config.get('CLIENT_TOKEN')
    data_ = {'token': token,'no_test_uploaded_files':no_test_uploaded_files,'ave_uploaded_time':ave_upload_time }
    results=_connect_Warwick_get_resp(url, data_)
    _modify_config_file('HAS_BEEN_TESTED','Yes')
    return results


def get_process_output(db, job_id=45):
    job = d_util.get_job(job_id, db)
    if not job.job_data:
        return
    print (job.job_data.keys())
    print("=======================================")
    print(job.job_data)
    print("---------------------------")
    job_data = job.job_data
    print("=======================================")
    out = job_data.get('stdout')
    fail= job_data.get('stderr')
    print (fail)
    print (out)
    output = out.split('{')
    try:
        assembly_status = json.loads(('{' + output[-1]))
        print(assembly_status)
    except:
        print ("Error..")
        print (output)
        print ("===================")
        print (fail)

