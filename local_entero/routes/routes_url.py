"""
Purpose     : This moudles contains the applcation routes

| Revision History:
| Name:version 0.1
| Date: 02/2020
| Description: deployed in DSMZ enterobase

"""

import sys
import os
import requests
import json
from . import routes
from flask import Flask, render_template,request, redirect, session, jsonify, url_for, flash, send_file, make_response
from flask_login import login_user, logout_user, login_required, current_user
from local_entero import local_app
from local_entero.forms import configurations_form, strain_metadata_forms
import local_entero.databases.databases_utilities as d_util
import local_entero.routes.routes_utility_methods as r_util_methods
from local_entero import db, warwick_enterobase


@local_app.context_processor
def get_instance_name_reg_status():
    IS_RESGISTERED=local_app.config.get('IS_RESGISTERED')

    if not IS_RESGISTERED or IS_RESGISTERED==False:
        is_register_request_submitted=False
    elif IS_RESGISTERED==True:
        is_register_request_submitted=True
    instance_name = local_app.config.get('INSTANCE_NAME')
    has_Been_test=local_app.config.get('HAS_BEEN_TESTED')
    if not is_register_request_submitted:
        it_is_not_registered=True
    elif local_app.config.get('CLIENT_TOKEN')==local_app.config.get('CLIENT_ID'):
        it_is_not_registered=False
    else:
        it_is_not_registered=True

    return dict(instance_name=instance_name, it_is_not_registered=it_is_not_registered, is_register_request_submitted=is_register_request_submitted, has_Been_test=has_Been_test)

@routes.route('/get_icon')
def get_icon():
    instance_icon = local_app.config.get('INSTANCE_ICON')
    local_app.logger.info ("Intstance icon is: "+ instance_icon)
    if not instance_icon or not os.path.isfile(instance_icon):
        local_app.logger.error("Could not find instance icon file : %s"%instance_icon)
        return None
    return send_file(instance_icon,  cache_timeout=0)

@routes.route('/')
def main():
    if 'token' in session:
        del session['token']
    if local_app.config.get('NEED_RESTART'):
        return render_template("empty.html", title="Local EnteroBase")
    if local_app.config.get('Error_message'):
        flash(local_app.config['Error_message'])
        return redirect(url_for('routes.update_system_configuration'))


    try:
        if 'warwick_enterobase_token' in session:
            if local_app.config.get('IS_RESGISTERED') == True and local_app.config.get(
                    'CLIENT_TOKEN') == local_app.config.get('CLIENT_ID'):
                try:
                    me = warwick_enterobase.get('/oauth/userinfo')
                    if me.data.get('error'):
                        flash (me.data.get('error'))
                        return redirect(url_for('routes.login'))
                    return redirect(url_for("routes.index"))
                except Exception as e:
                    if 'warwick_enterobase_token' in session:
                        del session['warwick_enterobase_token']
                    local_app.logger.error("Error in main, error message: %s"%e)

        return render_template("main.html", title="Local EnteroBase")
    except Exception as e:
        local_app.logger.error("authorized error: %s "%e)
        flash( "Error: "+str(e))
        return render_template("main.html", title="Local EnteroBase")



@routes.route('/index')
@login_required
def index():
    """
    The actual main webpage
    It contains all the available databases which the user can access in Warwick EnteroBase
    so, it first calls Warwick EnteroBase to get the user's databases.

    """
    databases_=get_active_databases()
    #sort databases list acording to its no of strains
    databases=sorted(databases_, key = lambda i: i['total_strains'], reverse=True)
    if isinstance(databases, list):
        session['databases'] = databases
        return render_template("index.html", title="Available Databases", databases=databases)
    return redirect(url_for('routes.logout') )


def check_client_registration ():
    params_={}
    params_['client_id']=local_app.config.get('CLIENT_ID')
    params_['client_secret']=local_app.config.get('LIENT_SECRET')
    params_['redirect_uri']=url_for('routes.authorized', _external=True,_scheme='https')
    r = requests.get(
        local_app.config.get('BASE_URL') + "/oauth/is_client_registered",
        params=params_)
    return r

@routes.route('/login')
def login():
    if local_app.config.get('NEED_RESTART'):
        return render_template("empty.html", title="Local EnteroBase")

    return warwick_enterobase.authorize(callback=url_for('routes.authorized', _external=True,_scheme='https'))


@routes.route('/authorized', methods=['GET', 'POST'])
def authorized():
    try:
        # connect to Warwick EnteroBase to get the user token
        resp = warwick_enterobase.authorized_response()
        # check if there is an access denied for the user
        if resp is None:
            return 'Access denied: reason=%s error=%s' % (
                request.args['error_reason'],
                request.args['error_description']
            )
        # save user token to a session
        token=(resp['access_token'], '')
        session['warwick_enterobase_token'] = (resp['access_token'], '')
        # query Warwick to get user information
        warwick_user_info = warwick_enterobase.get('/oauth/userinfo')

        id = warwick_user_info.data.get('id')
        email = warwick_user_info.data.get("email")
        usersname = warwick_user_info.data.get("username")
        is_admin=warwick_user_info.data.get("is_local_admin")
        # check if the user in the local database
        # if not add it
        user=d_util.check_user(id, usersname, email, is_admin, db)
        # Begin user session by logging the user in
        login_user(user)
        # Send user to the main homepage
        return redirect(url_for("routes.index"))
    except Exception as e :
        local_app.logger.error("authorized error: %s "%e)
        flash ("Authorization error: %s "%e+"Please contact system adminstrator")
        local_app.config['Error_message']="Authorization error: %s "%e
        return redirect(url_for("routes.main"))


@routes.route('/logout')
def logout():
    try:
        if 'warwick_enterobase_token' in session:
            warwick_enterobase.get('/oauth/revoke_token')
            del session['warwick_enterobase_token']
    except:
        local_app.logger.error("authorized error: %s "%sys.exc_info()[0])
    if 'databases' in session:
        del session['databases']
    if 'databases' in session:
        del session['databases']
    logout_user()
    return render_template("main.html", title="Local EnteroBase")

@warwick_enterobase.tokengetter
def get_warwick_enterobase_oauth_token():
    return session.get('warwick_enterobase_token')


@routes.route('/register_client', methods=['POST','GET'])
def register_client():
    #ToDO, add a check to be sure that it is submitted one time
    if current_user.is_anonymous:
        token = session.get('token')
        if token:
            if not r_util_methods.compare_encrypt_text(token, local_app.config.get('APP_PASSWORD')):
                del session['token']
                token = None
        if not token:
            if local_app.config.get('APP_PASSWORD') == None:
                flash(
                    "Administrator password is not configured yet, please ask the system administrator to set up one, it is required to access register client page.")
                # return redirect(url_for("routes.main"))
            return redirect(
                url_for("routes.local_authorizarion", next_url=url_for('routes.register_client')))
    elif not current_user.is_admin:
        flash(("You are not authorized to access register client page"))
        return redirect(url_for("routes.main"))
    client_reg_form = configurations_form.Register_client_Form()
    if client_reg_form.validate_on_submit():
        data = request.form.to_dict(flat=True)
        params={}
        params['client_uri']=client_reg_form.Local_URL.data
        params['client_name']=client_reg_form.Local_Name.data
        params['description']=client_reg_form.Local_Description.data
        if params['client_uri'].endswith('/'):
            params['client_uri'] = params['client_uri'][:-1]             

        enterobase_register_address =local_app.config.get('REQUEST_CLIENT_REGISTER')
        params_ = json.dumps(params)
        if 'token' in session:
            del session['token']
        return redirect(enterobase_register_address+"?data="+params_)
    else:
        # It is not valid form
        # Erros will be collected and logged
        errors=r_util_methods.get_form_validation_error(client_reg_form)
        local_app.logger.error("register_client validation error: %s "%errors)


    return render_template("forms/register_instance.html", client_reg_form=client_reg_form)

@routes.route('/check_login', methods=['POST', 'GET'])
def check_app_log():
    logForm=configurations_form.LogForm()
    if logForm.validate_on_submit():
        pass
    return render_template("forms/app_log_page.html", logForm=logForm)


@routes.route('/local_authorizarion', methods=['POST', 'GET'])
def local_authorizarion():
    if not local_app.config.get('APP_PASSWORD'):
        flash(
            "Administrator password is not configured yet, please ask the system administrator to set up one, it is required to access authorize your access.")
    next_url = request.args.get('next_url')
    logForm = configurations_form.LogForm(data={'next_url':next_url})
    if logForm.validate_on_submit():
        data = request.form.to_dict()
        session['token']=data['Application_Password']
        resp=data['next_url']
        if resp:
            return redirect (resp)
        else:
            return redirect(url_for('routes.main'))
    return render_template("forms/local_autorization.html", logForm=logForm)

@routes.route('/update_system_configuration', methods=['POST', 'GET'])
def update_system_configuration():
    """
    The local adminstrator can change the system configuration but after that
    he needs to restart the application otherwise the application will be functionless.

    """
    if current_user.is_anonymous:
        token=session.get('token')
        if token:
            if not r_util_methods.compare_encrypt_text(token, local_app.config.get('APP_PASSWORD')):
                del session['token']
                token=None
        if not token:
            if local_app.config.get('APP_PASSWORD')==None:
                flash("Administrator password is not configured yet, please ask the system administrator to set up one, it is required to access system configuration page.")
                #return redirect(url_for("routes.main"))
            return redirect(url_for("routes.local_authorizarion", next_url=url_for('routes.update_system_configuration')))
    elif not current_user.is_admin:
        flash(("You are not authorized to access system configuration page"))
        return redirect(url_for("routes.main"))

    sys_config_form = configurations_form.create_configuration_form()

    if sys_config_form.validate_on_submit():
        data = request.form.to_dict()
        if 'csrf_token' in data:
            del data ['csrf_token']
        data['ALLOWED_PROCESS']=int(data['ALLOWED_PROCESS'])
        data['DATABASE_PORT'] = int(data['DATABASE_PORT'])
        if r_util_methods.check_config_changes(data):
            flash("You have changed system configuration, please restart the server")
            local_app.config['Error_message']=''
            local_app.config['NEED_RESTART']=True
        if 'token' in session:
            del session['token']
        return redirect(url_for("routes.main"))
    else:
        errors = r_util_methods.get_form_validation_error(sys_config_form)
        local_app.logger.error("update_system_configuration validation error: %s " % errors)

    return render_template("forms/system_config.html", sys_config_form=sys_config_form)


@routes.route('/edit_strain_data', methods=["GET", "POST"])
@login_required
def edit_strain_data():
    """
    Modify single strains metadata.

    """
    id= request.args.get('id')
    if not id:
        message="No strain id is provided ..."
        return create_results_message("Error", message)
    database = request.args.get('database')
    databases = session.get('databases', None)
    if not database:
        database = session.get('database', None)
    else:
        database = session['database'] = database

    res2=r_util_methods.get_strain_data_to_edit(id)
    strain_metadata=_get_strain_metadata(database)
    try:
        res = json.loads(strain_metadata)
        if isinstance(res, str):
            return res
    except:
        return strain_metadata
    column_no, strain_metadata_from = strain_metadata_forms.create_strain_metedata_form(json.loads(strain_metadata))
    user_files_list=d_util.get_user_read_files(current_user.id, db)

    return render_template("database.html", database=database, strain_metadata_from=strain_metadata_from,
                            title=database, databases=databases,user_files_list=user_files_list,
                           continents_countries=local_app.config.get('CONTINENTS_COUNTRIES'))

    return "DONE: "+database

@routes.route('/update_read_files', methods=["GET", "POST"])
@login_required
def update_read_files():
    """
    replace read files for single saved starin.

    """
    database = request.args.get('database')
    if not database:
        database=session.get('database', None)
    else:
        database= session['database']=database
    st_id= request.args.get('id')
    strain_name=request.args.get('strain')
    if not st_id or not strain_name:
        # ToDo return message which has more details
        return "Missing starin id and/ stran name "
    user_files_list = d_util.get_user_read_files(current_user.id, db,st_id)

    databases = session.get('databases', None)
    title = r_util_methods.get_database_title(databases, database)
    reads_form=strain_metadata_forms.Read_files_Form()
    if reads_form.validate_on_submit():
        st_id=request.form.get('st_id')
        r_util_methods.update_read_files(reads_form,database, current_user.id, current_user.enterobase_id, st_id)
        return redirect(url_for("routes.get_my_jobs_status", database=database, title=title, databases=databases, ))

    else:
        errors = r_util_methods.get_form_validation_error(reads_form)
        local_app.logger.error("update_read_files validation error: %s " % errors)

    return render_template('update_read_files.html', title=title, databases=databases, strain_name=strain_name, reads_form=reads_form, user_files_list=user_files_list, st_id=st_id)


@routes.route('/user_jobs_status', methods=["GET", "POST"])
@login_required
def get_my_jobs_status():
    """
    open user jobs status web page.

    """
    database = request.args.get('database')
    title = request.args.get('title')
    if not database:
        database = session.get('database', None)
    else:
        database = session['database'] = database
    databases = session.get('databases', None)
    if not title:
        title = r_util_methods.get_database_title(databases, database)
    columns=r_util_methods.get_jobs_status_column()
    data=r_util_methods.get_user_jobs_status(current_user.id, database)
    return render_template('databases/user_jobs_status.html', title=title, databases=databases, data=data, columns=columns)


def _get_strains_meta_form():
    databases = session.get('databases', None)
    database = request.args.get('database')
    if not database:
        database = session.get('database', None)
    else:
        database = session['database'] = database

    title = request.args.get('title')
    if not database:
        return json.dumps(create_results_message("No database is provided"))
    '''
    create strain metada form '''
    if not title:
        title = r_util_methods.get_database_title(databases, database)
    strain_metadata = _get_strain_metadata(database)
    try:
        res_data = json.loads(strain_metadata)
        if isinstance(res_data, str):
            return res_data
        elif isinstance(res_data, dict):
            if res_data['Error']:
                return res_data['Message']
    except Exception as e:
        return strain_metadata
    column_no, strain_metadata_from = strain_metadata_forms.create_strain_metedata_form(res_data)
    return column_no, strain_metadata_from, databases, database, title

@routes.route('/save_strain_raw_data', methods=['POST', 'GET'])
@login_required
def save_starin_raw_data():
    """
    save strain metadata to database used with "upload single strains" option.

    """
    column_no, strain_metadata_from, databases, database, title = _get_strains_meta_form()
    if strain_metadata_from.validate_on_submit():
        database = request.args.get('database')
        if not database:
            database = session.get('database', None)

        r_util_methods.add_raw_strain_data_jobs_2(request, strain_metadata_from, database, current_user.id, current_user.enterobase_id)

        return jsonify(data={'message': 'ok {}'.format(strain_metadata_from.data)})
    else:
        errors = r_util_methods.get_form_validation_error(strain_metadata_from)
        local_app.logger.error("save_starin_raw_data validation error: %s " % errors)

    return jsonify({'error':errors})

@routes.route('/upload_reads', methods=['POST', 'GET'])
@login_required
def upload_reads_database_page():
    """
    This is used to upload multiple strains metadata
    it uses javascript library (slickgrid) to display the table
    It is not Fully implemented yet

    * calling EnteroBase API (not implemented in EnteroBase yet) to help in filling and validating some fields e.g. location
    * uploading read files.

    """
    user_files_list=d_util.get_user_read_files(current_user.id, db)

    databases = session.get('databases', None)
    if not databases:
        databases=get_active_databases()
    database = request.args.get('database')
    if not database:
        database = session.get('database', None)
    else:
        database = session['database'] = database

    title = request.args.get('title')
    if not database:
        return json.dumps(create_results_message("No database is provided"))

    if not title:
        title = r_util_methods.get_database_title(databases, database)

    strain_metadata = json.loads(_get_strain_metadata(database))
    grid_metadata_columns, data,req_fields=r_util_methods.get_grid_starins_metadata_columns(strain_metadata)

    return render_template("databases/user_strains_upload.html",database=database, columns=grid_metadata_columns,data=data, req_fields=req_fields,title=title, databases=databases, user_files_list=user_files_list, continents_countries=local_app.config.get('CONTINENTS_COUNTRIES'))


#############################################################
@routes.route('/database', methods=['POST', 'GET'])
@login_required
def database_page():
    """
    Open databse page it should called with database arg
    if no database is provided, it wiill look at the session
    to reterive the active database.

    """
    database = request.args.get('database')
    if not database:
        database = session.get('database', None)
    else:
        database = session['database'] = database

    user_files_list=d_util.get_user_read_files_database(current_user.id, db, database)
    column_no, strain_metadata_from, databases, database, title=_get_strains_meta_form()
    if strain_metadata_from.validate_on_submit():
        #the submission is overided by javascript calls to deperate saving the metada and uploaing the read files
        return "Done"
        r_util_methods.add_raw_strain_data_jobs(strain_metadata_from, database, current_user.id, current_user.enterobase_id, release_period)
        #return redirect (url_for('routes.database_page', database=database, title=title))
        #return redirect(url_for("routes.get_user_jobs_status", database=database, title=title, databases=databases, ))

    else:
        print ("It is not valid ... because:")
        errors = r_util_methods.get_form_validation_error(strain_metadata_from)
        local_app.logger.error("database_page error: %s " % errors)


    # ToDo get the labels from the form automatically
    strainMetadataGridlabels=["Name","Read file 1", "Read File 2", "Collection Date"]
    return render_template("database.html",database=database, strain_metadata_from=strain_metadata_from,title=title, databases=databases, user_files_list=user_files_list, continents_countries=local_app.config.get('CONTINENTS_COUNTRIES'))


@routes.route('/get_active_databases', methods=['GET', 'POST'])
@login_required
#I think this should not be exposed to the user directory,
def get_active_databases():
    """
    Check if the active databases are saved in the sesssion
    if not call EnteroBase and get the databases which the user has access to.

    """
    if 'warwick_enterobase_token'not in session:
        return redirect(url_for('routes.login',next_url=url_for('routes.get_active_databases') ))
    try:
        resp = warwick_enterobase.get('/get_user_active_databases')
    except Exception as e:
        local_app.logger.error("Error while connecting Warwick EnteroBase to get the active databases")
        return []
    return json.loads((resp.raw_data))

@routes.route('/get_strain_metadata_columns', methods=["GET", "POST"])
def get_strain_metadata_columns():
    """
    Returns the field information for the strains grid for a database.

    """
    if len(request.args)==0:
        return json.dumps(create_results_message(True, message="No database is provided"))
    database = request.args.get('database')

    if not database :
        return json.dumps(create_results_message(True, message="No database is provided"))

    return _get_strain_metadata(database)

def _get_strain_metadata(database):
    """
    Call EnteroBase to get the strans metadata column for the database.

    """
    c_url=r'/species/%s/get_strain_columns' % database
    try:
        resp = warwick_enterobase.get(c_url)
        return (resp.raw_data)
    except Exception as e:
        local_app.logger.error("get strain metada data error, messsage: %s"%str(e))
        print ("ERROR, please try later")
        return json.dumps(create_results_message(True, message="database is not valid"))

@routes.route('/upload_files', methods=['POST', 'GET'])
@login_required
def upload_files():
    """
    Upload strain read file
    and return dict which contains the uploaded results
    todo: create a function to return MD5 for the file in javascript level
    so, it can be compared by the front end to be sure the file is uploaded sucessfully.

    """
    # check if the post request has the file part
    if 'files[]' not in request.files:
        resp = jsonify({'message': 'No file part in the request'})
        resp.status_code = 400
        return resp
    files = request.files.getlist('files[]')
    database = request.args.get('database')
    if not database:
        database = session.get('database', None)
    resp, res=r_util_methods.upload_file (files, database, current_user.enterobase_id)
    return resp


@routes.route('/upload_files_2', methods=['POST', 'GET'])
def upload_files_2():
    """
    check if the post request has the file part
    this methos intended to be used with slickgrid upload metadata version
    for uploading multiple strains
    It is not working yet

    """
    if 'readfiles[]' not in request.files:
        resp = jsonify({'message': 'No file part in the request'})
        resp.status_code = 400
        return resp

    files = request.files.getlist('readfiles[]')

    database = request.args.get('database')
    if not database:
        database = session.get('database', None)
    resp, res=r_util_methods.upload_file (files, database, current_user.enterobase_id)
    return resp

#############################

@routes.route('/check_base_upload_file', methods=['POST', 'GET'])
def check_base_upload_file():
    """
    create a base file for the test
    It will be used to create each of the 100 files which are used for testing uploads.

    """
    try:
        r_util_methods._create_base_test_file()
        res=True
    except:
        res=False
    return json.dumps({'results':res})


@routes.route('/test_upload_file', methods=['POST', 'GET'])
def test_upload_file():
    """
    receieve a rquest to upload test file to Warwick EnteroBase.

    :return: a dict contains the upload result and the upload time
    """
    try:
        res, elapsedTime=r_util_methods._creat_file_test_upload()
        if res=='true':
            results='successful'
        else:
            results='failed'
    except:
        results = 'failed'
        elapsedTime='-'
    return json.dumps({'results': results, "elapsedTime": elapsedTime})

@routes.route('/test_upload_files', methods=['POST', 'GET'])
def test_upload_files():
    """
    Load the web page for the test upload files.

    """
    if current_user.is_anonymous:
        token = session.get('token')
        if token:
            if not r_util_methods.compare_encrypt_text(token):
                del session['token']
                token = None
        if not token:
            if local_app.config.get('APP_PASSWORD') == None:
                flash("Administrator password is not configured yet, please ask the system administrator to set up one, it is required to access test upload files page.")
                #return redirect(url_for("routes.main"))
            return redirect(
                url_for("routes.local_authorizarion", next_url=url_for('routes.test_upload_files')))

    elif not current_user.is_admin:
        flash(("You are not authorized to access test upload files page"))
        return redirect(url_for("routes.main"))


    return render_template("test_client_upload.html")

@routes.route('/submit_test_upload_files', methods=['POST', 'GET'])
def submit_test_upload_files():
    return r_util_methods.submit_upload_test_results(request.form.get('no_uploaded_files'), request.form.get('average_time'))


@routes.route('/help', methods=['GET'])
def help():
    return render_template("empty.html")
    #return "Not implemented yet"

def create_results_message(res,message=None):
    """
    create standard result message.

    :param res: Boolean take false or true
    :param message: message passed to the front end
    :return: dictonary which contains the results
    """
    results={"Error":res}
    if message:
        results["Message"]=message
    return results


