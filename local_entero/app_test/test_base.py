"""
Purpose     : create test unit for the application

Special Notes: it tests create the app, create database insert/delete rows to/from the tables


| Revision History:
| Name:version 0.1
| Date: 02/2020
| Description: deployed at DSMZ

"""

import os
from flask_testing import LiveServerTestCase
import urllib.request
import unittest

app_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
#sys.path.append(app_dir)

from local_entero  import db,create_app as app_creator

from local_entero.databases.databases_models import Users

import local_entero.databases.databases_utilities as d_util

from test_data import get_test_data


'''
This the main test module which should be used to test the application main functions'''
class BasicTestCase(LiveServerTestCase):
    """A for the server is up.
    """

    def create_app(self):
        local_app,cel=app_creator('testing')
        return local_app

    def setUp(self):
        """
        Will be called before every test.

        """
        db.drop_all()
        db.create_all()
         # create test admin user
        admin_data = {"enterobase_id" :-1, "token":"aafdsfdssdfsfdsdfgdfgfddfggfd", "is_admin":True, "username": "toto"}
        d_util.add_user(db, admin_data)
        admin = Users(**admin_data)
        print (admin.is_admin)
        d_util.add_jobs(get_test_data(),db, 1)


    def tearDown(self):
        """
        Will be called after every test

        """
        db.session.remove()
        db.drop_all()

    def test_server_is_up_and_running(self):
        response = urllib.request.urlopen(self.get_server_url()+'/')
        self.assertEqual(response.code, 200)

    def test_get_user(self):
        user = d_util.get_user(1, db)
        self.assertNotEqual(user, None)

    def test_get_jobs(self):
        jobs = d_util.get_all_jobs(db)
        self.assertEqual(len(jobs), 5)

if __name__ == '__main__':
    unittest.main()