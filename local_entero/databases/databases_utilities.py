"""
Purpose     : database utilities methods, e.g.create dbasebase and table, SIDU,
             i.e. insert, update delete and select rows to/from jobs, users, starins metadadata tables


| Revision History:
| Name:version 0.1
| Date: 02/2020
| Description: deployed

"""

from sqlalchemy_utils import database_exists, create_database
from psycopg2.extras import RealDictCursor
import psycopg2
from flask import current_app
import datetime
from local_entero.databases.databases_models import Users, Jobs, Strain_Data, UsersAssemblyReadsFiles
from datetime import datetime
import os

# This methods are working with postgresql
# I am not sure it is needed to modify it if the user wants to use another database server in case of we allow him to do
def sql_query_database(sql):
    db_conn = None
    try:
        db_conn = psycopg2.connect(current_app.config['SQLALCHEMY_DATABASE_URI'])
        db_cursor = db_conn.cursor(cursor_factory=RealDictCursor)
        db_cursor.execute(sql)
        results = db_cursor.fetchall()
        db_conn.close()
        return results

    except Exception as e:
        if db_conn:
            db_conn.rollback()
            db_conn.close()
        current_app.logger.exception("The following query failed:%s" % sql)
        return None

# create the database
def create_main_database(db, local_app, config):
    config.SQLALCHEMY_BINDS = dict(jobs_db=config.SQLALCHEMY_DATABASE_URI)
    db.init_app(local_app)
    check_databases(db)
    return db

def check_databases(db):
    """
    check if the database exist
    if not it will create it

    :param db:
    """
    engine = db.engine
    if not database_exists(engine.url):
        create_database(engine.url)


def add_jobs(all_data, db, user_id):
    for data in all_data:
        data['date_received']=datetime.now()
        data['status']='Received'
        data['user_id']=user_id
        job=Jobs(**data)
        db.session.add(job)
    db.session.commit()
    db.session.close()

def add_job(data,db):
    data['date_received'] = datetime.now()
    data['status'] = 'Received'
    job = Jobs(**data)
    db.session.add(job)
    db.session.commit()
    job_id=job.id
    db.session.close()
    return job_id


def get_job(job_id,db):
    job = db.session.query(Jobs).filter(Jobs.id==job_id).one()
    db.session.close()
    return job


def get_user_jobs(db, database, user_id):
    jobs=db.session.query(Jobs).filter(Jobs.user_id==user_id).filter(Jobs.database==database).all()
    db.session.close()
    return jobs


def get_jobs_status(job_ids, db):
    jobs = db.session.query(Jobs).filter(Jobs.id.in_(job_ids)).all()
    #db.session.close()
    return jobs

def get_jobs_for_status(status, db):
    jobs = db.session.query(Jobs).filter(status=status).all()
    #db.session.close()
    return jobs


def get_all_jobs(db):
    jobs = db.session.query(Jobs).all()
    #db.session.close()
    return jobs

def get_strains_data_job(strain_data_id, db):
    job = db.session.query(Jobs.id).filter(Jobs.strain_data_id== strain_data_id).one()
    j_id= job.id
    db.session.close()
    return j_id


def update_job(job_id, modified_data, db, jobs_results=None):
    job=db.session.query(Jobs).filter(Jobs.id==job_id).one()
    st_id=job.strain_data_id
    print (st_id)

    modified_data['last_updated']=datetime.now()
    for key, value in modified_data.items():
        setattr(job, key, value)
    if jobs_results:
        job.job_data=jobs_results
    db.session.commit()
    #job.update(modified_data)
    return st_id


def update_strain_data(st_id, modified_data, db):
    strain_data=db.session.query(Strain_Data).filter(Strain_Data.id==st_id).one()
    for key, value in modified_data.items():
        setattr(strain_data, key, value)
    if modified_data.get('status'):
        if modified_data.get('status')=="Running":
            strain_data.status = 'Processing'
        else:
            strain_data.status=modified_data['status']
    strain_data.aasembly_file=modified_data.get('output_location')
    db.session.commit()
    #db.session.close()

def get_queued_jobs(db,JOBS):
    jobs = db.session.query(JOBS).filter(JOBS.status=='queued').one().all()
    db.session.close()
    return jobs


def add_user(db, user_):
    user = Users(**user_)
    # save the user to the table
    db.session.add(user)
    db.session.commit()
    #db.session.close()
    return user

def delete_user(db, User, user_id):
    pass

def update_user(db, User, user):
    pass

def get_user(user_id, db):
    user = db.session.query(Users).filter(Users.id == user_id).one()
    return user

def get_user_using_warwick_id(enterobase_id, db):
    users = db.session.query(Users).filter(Users.enterobase_id == enterobase_id).all()
    return users


def  modify_raw_strain_(modified_fields, st_id, db):
    strain_date = db.session.query(Strain_Data).filter(Strain_Data.id == int(st_id)).one()
    for key, value in modified_fields.items():
        setattr(strain_date, key, value)
    db.session.commit()
    db.session.close()

def get_raw_strain(id, db):
    result=db.session.query(Strain_Data).filter(Strain_Data.id==id).one()
    res=result.as_dict()
    return res


def add_raw_strain_(data, database, user_id, db):
    raw_strain=Strain_Data()
    raw_strain.data=data
    raw_strain.strain= data.get('strain')
    raw_strain.release_period=data.get('release_period')
    raw_strain.database=database
    raw_strain.user_id=user_id
    raw_strain.status="Uploaded"
    raw_strain.date_uploaded=datetime.now()
    db.session.add(raw_strain)
    db.session.commit()
    st_id=raw_strain.id
    db.session.close()
    return st_id

def add_read_files_records(data_r1, data_r2, db):
    usersAssemblyReadsFile_1=UsersAssemblyReadsFiles(**data_r1)
    db.session.add(usersAssemblyReadsFile_1)
    usersAssemblyReadsFile_2 = UsersAssemblyReadsFiles(**data_r2)
    db.session.add(usersAssemblyReadsFile_2)
    db.session.commit()
    db.session.close()

def update_read_file_status(filelocation, status, db):
    read_file=db.session.query(UsersAssemblyReadsFiles).filter(UsersAssemblyReadsFiles.file_location==filelocation).one()
    st_id =read_file.strain_data_id
    read_file.status=status
    db.session.commit()
    db.session.close()
    return st_id


def get_strain_read_files_status(st_id, db):
    read_files = db.session.query(UsersAssemblyReadsFiles).filter(UsersAssemblyReadsFiles.strain_data_id == st_id).all()
    status=[]
    for read_file in read_files:
        status.append(read_file.status)
    return status


def get_user_strains_data(db, database, user_id):
    user_strains_data=db.session.query(Strain_Data).filter(Strain_Data.user_id==user_id).filter(Strain_Data.database==database).all()
    db.session.close()
    return user_strains_data

def get_user_read_files_database(user_id, db, database):
    sts_data = db.session.query(Strain_Data.data, Strain_Data.id).filter(Strain_Data.user_id == user_id).filter(Strain_Data.database==database).all()
    read_files = []
    for st_data in sts_data:
        fls = st_data.data.get("client_Readfiles")
        fls = fls.split(",")
        read_files = read_files + fls
    return read_files


def get_user_read_files(user_id, db, s_id=None):
    sts_data=db.session.query(Strain_Data.data, Strain_Data.id).filter(Strain_Data.user_id==user_id).all()
    read_files=[]
    print (type(s_id));
    for st_data in sts_data:
        if str(st_data.id)==s_id:
            print ("read_files", st_data.data.get("client_Readfiles"))
            continue
        fls=st_data.data.get("client_Readfiles")
        fls = fls.split(",")
        read_files=read_files+fls
    return read_files

def check_user(enterobase_id, usersname, email, is_admin, db):
    # Doesn't exist? Add it to the database.
    users=get_user_using_warwick_id(enterobase_id, db)
    if len(users)==0:
        user_= {'enterobase_id': enterobase_id, 'username': usersname, 'email': email, 'is_admin': is_admin}
        user=add_user(db, user_)
    else:
        user=users[0]
        if user.is_admin!=is_admin:
            user.is_admin=is_admin
            db.session.commit()

    return user

