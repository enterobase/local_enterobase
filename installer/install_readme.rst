Automatic installation using the script
=======================================
The script will automatically download, configure and run the system singularity images (i.e. Nginx, PostgreSql and Gunicorn and local EnteroBase) and their dependencies packages.
During the installation the user will be asked to:

- Provide his server domain name or IP address
- Set a password for local EnteroBase.
Then system components will be configuring using the default parameters and should run without any issue.

