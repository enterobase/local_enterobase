#!/bin/bash

#==========================================================#
# PURPOSE     : install the packages and the singularity   #
#               images, configure and run the system       #
#               containers                                 #
#               In addition:                               #
#                -It creates sh files and text             #
#                 files to confgure the conatiners and     #
#                 run them                                 #
#                - It configures the host machine to run   #
#                 the containers when the machine starts up#
#                                                          #
# SPECIAL NOTES: The script needs more test to be sure     #
#                 it is working in dffirent machines       #
#==========================================================#


if ! [ $(id -u) = 0 ]; then
    echo
    echo "$(tput setaf 1)The script needs root privileges, please run the script with sudo.$(tput sgr0)";
    echo

   exit 1
fi

if [ $SUDO_USER ]; then
    real_user=$SUDO_USER

else
    real_user=$(whoami)

fi



echo
echo "****************************************************************"
echo "This script will install the packages and the singularity images"
echo "which are required to run the local EnteroBase on your machine."
echo "****************************************************************"
echo
read -r -p "Are you sure you want to continue? [y/n] " input

case $input in
    [yY][eE][sS]|[yY])
		echo "Yes"
		;;
    [nN][oO]|[nN])
		echo "No"
                exit
       		;;
    *)
	echo "Invalid input..."
	exit 1
	;;
esac

#create folders for the diffrent system containers

local_enterobase_home="/home/$real_user/local_enterobase_home"
nginx_folder="$local_enterobase_home/nginx"
postgres_folder="$local_enterobase_home/postgres"
local_enterobase_folder="$local_enterobase_home/local_enterobase"
current_folder=`pwd`

if ! test -d $local_enterobase_home; then sudo -u $real_user mkdir $local_enterobase_home; fi
if ! test -d $nginx_folder; then sudo -u $real_user mkdir $nginx_folder; fi

if ! test -d $postgres_folder; then sudo -u $real_user mkdir $postgres_folder; fi
if ! test -d $postgres_folder/postgres_data; then sudo -u $real_user mkdir $postgres_folder/postgres_data; fi
if ! test -d $postgres_folder/postgres_temp; then sudo -u $real_user mkdir $postgres_folder/postgres_temp; fi

if ! test -d $local_enterobase_folder; then sudo -u $real_user mkdir $local_enterobase_folder; fi


echo $local_enterobase_home


echo "check if singularity is installed in the machine"

if which singularity >/dev/null; then
    echo
    version=`singularity --version`
    ##toDo check the singularity version to be sure it is >3
    echo "************************************"
    echo "* $version is already installed *"
    echo "************************************"
    echo
 else
    echo "****************************"
    echo "* Singularity installation *"
    echo "****************************"
    echo
    echo "Install the required packages"
    echo "-----------------------------"
    sudo apt-get update

    sudo apt-get install -y \
        build-essential \
        libssl-dev \
        uuid-dev \
        libgpgme11-dev \
        squashfs-tools \
        libseccomp-dev \
        wget \
        pkg-config \
        git \
        cryptsetup
    echo
    echo "Install go which is used by the singularity container"
    echo "------------------------------------------------------"
    export VERSION=1.14.2 OS=linux ARCH=amd64
    sudo -u $real_user  wget https://dl.google.com/go/go$VERSION.$OS-$ARCH.tar.gz
    sudo tar -C /usr/local -xzvf go$VERSION.$OS-$ARCH.tar.gz
    sudo -u $real_user rm go$VERSION.$OS-$ARCH.tar.gz
    sudo ln -s /usr/local/go/bin/go /usr/local/bin/go
    echo
    echo
    echo "Download Singularity from a release"
    echo "------------------------------------"
    export VERSION=3.5.2 &&
    sudo -u $real_user wget  https://github.com/sylabs/singularity/releases/download/v${VERSION}/singularity-${VERSION}.tar.gz
    sudo -u $real_user tar -xzf singularity-${VERSION}.tar.gz
    cd singularity
    echo pwd
    echo "make configuration"
    sudo -u $real_user ./mconfig
    sudo -u $real_user make -C builddir
    echo "Make build"
    sudo make -C builddir install
fi
echo
echo
echo "********************************************"
echo "* Downloading and Running NGINX Web Server *"
echo "*******************************************"
echo
echo "Pulling the singularity image file “nginx_container.sif” from the cloud library and save it to:"
echo "$nginx_folder"
sudo -u $real_user singularity pull --force  --arch amd64    $nginx_folder/nginx_container.sif library://local_enterobase/default/nginx_local_enterobase:0.1
echo
echo "Copying nginx configuration file and certs folder to $nginx_folder"
echo
echo
#nginx_local_enterobase.conf


if  test -f "$nginx_folder/nginx_local_enterobase.conf"; then sudo -u $real_user mv $nginx_folder/nginx_local_enterobase.conf $nginx_folder/nginx_local_enterobase.conf.back ; fi

sudo -u $real_user singularity run -B $nginx_folder:/home/nginx_user --app prep_nginx $nginx_folder/nginx_container.sif
echo "ask the user to input his domain name of ip addess to be used in nginx configuration file"

######'#######################################
# read domain name or ip address from the user#
###############################################

read_ip_address () {
while :
  do
  echo
  read -r -p "Please $1 you server IP or domian name: " server_address
  #check if it is valid ip address
      if [[ $server_address =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
      echo "Valid IP"
      break
    fi
     #cehck if it is valid domain name
     IFS='.' read -ra parts <<< "$server_address"
     length=${#parts[@]}



    case $length in
    3)

     if [[ "$server_address" =~ ^[A-Za-z0-9]+\.[A-Za-z0-9]+(\.[A-Za-z]{2,4})+$ ]]; then
       break;
    fi
    ;;
    2)
      if [[ "$server_address" =~ ^[A-Za-z0-9]+(\.[A-Za-z]{2,4})+$ ]]; then
      break;
       fi
    ;;
    4)

    if [[ "$server_address" =~ ^[A-Za-z0-9]+\.[A-Za-z0-9]+(\.[A-Za-z]{2,4})+$ ]]; then
      break;
    fi
    ;;

   esac
   echo
   echo  "$(tput setaf 1)\"$server_address\" is not vaild domain name or IP address$(tput sgr0)"
   done


  if [ $1 == "enter" ]; then
    server_1=$server_address
    fi

  if [ $1 == "re-enter" ]; then
    server_2=$server_address
    fi
  }


while :
do
read_ip_address "enter";

read_ip_address "re-enter" ;

if  [ "${server_1^^}" = "${server_2^^}" ]
          then
           break

           else
           echo
           #echo "###############################################";
           echo "$(tput setaf 1)Error, the server IP or domian name must be identical$(tput sgr0)";
           echo "$(tput setaf 1)You have entered \"$server_1\", \"$server_2\"$(tput sgr0)";
           #echo "###############################################";
           echo

fi

done

echo "Setting domain name: $server_1 for NGINX."
echo
sudo -u $real_user sed -i "s/replaced_by_your_server_uri/$server_1/g" $nginx_folder/nginx_local_enterobase.conf
echo

##############################################################################
#Writing nginx container configuration file and scrit to run the the service #
##############################################################################

#configuration file
cat > "$nginx_folder/nginx_cont.conf" <<EOF
#!/bin/bash
nginx_home=$nginx_folder
EOF

sudo chown $real_user:$real_user $nginx_folder/nginx_cont.conf
sudo chmod +x $nginx_folder/nginx_cont.conf


#test_run script
cat > "$nginx_folder/test_run_nginx.sh" <<EOF
#!/bin/bash
script_folder=\$(dirname \$(readlink -f \$0))
. \$script_folder/nginx_cont.conf
sudo singularity run   -B \$nginx_home:/home/nginx_user   --writable-tmpfs  --app test_nginx  nginx_container.sif
EOF

sudo chown $real_user:$real_user $nginx_folder/test_run_nginx.sh
sudo chmod +x  $nginx_folder/test_run_nginx.sh

#run service sciot
cat > "$nginx_folder/run_nginx.sh" <<EOF
#!/bin/bash
script_folder=\$(dirname \$(readlink -f \$0))
. \$script_folder/nginx_cont.conf
sing_list=\`sudo singularity instance list\`
#cehck if the instance is runing and if so it will stop it
if [[ \$sing_list =~ "nginx_sing" ]];
then
   echo "nginx is running ..!"
   sudo singularity instance stop nginx_sing
fi
sudo singularity instance start  -B \$script_folder:/home/nginx_user  --writable-tmpfs \$script_folder/nginx_container.sif nginx_sing
EOF

sudo chown $real_user:$real_user $nginx_folder/run_nginx.sh
sudo chmod +x  $nginx_folder/run_nginx.sh
echo
echo
cd $nginx_folder
sh test_run_nginx.sh
cd $current_folder
echo
echo
sudo bash $nginx_folder/run_nginx.sh
echo
############################################################################

echo
echo
echo "*****************************************************"
echo "* Downloading and Running PostgreSQL database Server *"
echo "*****************************************************"
echo
echo "Pulling the singularity image file “postgres_container.sif” from the cloud library and save it to:"
echo $postgres_folder
sudo -u $real_user singularity pull --force --arch amd64 $postgres_folder/postgres_container.sif  library://local_enterobase/default/postgres_local_enterobase:0.1
echo "running postgresql server"
echo

#. postgres.conf

#echo $password_pos
#write configuration and bash file
echo "wrting file$postgres_folder/postgres.conf"
echo
cat > "$postgres_folder/postgres.conf" <<EOF
#!/bin/bash
mypassword="local_password";
port_number=5432;
data_folder="$postgres_folder/postgres_data";
temp_folder="$postgres_folder/postgres_temp";
EOF

sudo chown $real_user:$real_user $postgres_folder/postgres.conf
sudo chmod +x $postgres_folder/postgres.conf

#sudo chgrp $real_user $postgres_folder/postgres.conf

echo "wrting file $postgres_folder/postgres.sh"
cat > "$postgres_folder/run_postgres.sh" <<EOF
#!/bin/bash
script_folder=\$(dirname \$(readlink -f \$0))
. \$script_folder/postgres.conf
sing_list=\`singularity instance list\`
#cehck if the instance is runing and if so it will stop it
if [[ \$sing_list =~ "postgres" ]];
then
   echo "singularity instance stop current postgres"
   singularity instance stop postgres
fi

echo "Running new postgres instance ..!"
SINGULARITYENV_POSTGRES_PASSWORD=local_password singularity instance start   -B \$data_folder:/var/lib/postgresql/data  -B \$temp_folder:/var/run/postgresql/   \$script_folder/postgres_container.sif   postgres  -p \$port_number
EOF

sudo chown $real_user:$real_user $postgres_folder/run_postgres.sh
sudo chmod +x  $postgres_folder/run_postgres.sh
echo
echo
sudo -u $real_user bash $postgres_folder/run_postgres.sh
echo
echo
echo "****************************************"
echo "* Running Gunicorn and the application *"
echo "****************************************"
echo
echo "Pulling the singularity image file “local_enterobase.sif” from the cloud library and save it to:"
echo $local_enterobase_folder
sudo -u $real_user singularity pull --force --arch amd64  $local_enterobase_folder/local_enterobase.sif  library://local_enterobase/default/local_enterobase:0.1
echo
echo "For security reason, you should first set up the system password so you can use the web interface to:"
echo "- Configure the application,"
echo "- Register your client with Warwick EnteroBase,"
echo "- Test upload files to Warwick EnteroBase."
echo
while :
  do
  read -sp 'Please enter Password: ' passvar
    echo
    read -sp 'Please confirm your password: ' passvar_c
    echo
   # echo $passvar, $passvar_c

    if [ $passvar == $passvar_c ];
          then
           break

           else
           echo
           echo "$(tput setaf 1)Error, passwords must be identical$(tput sgr0)"
           echo
    fi

  done
echo ""
echo "Setting password"
echo ""

sudo -u $real_user singularity run --app set_password $local_enterobase_folder/local_enterobase.sif -p $passvar

#run service sciot
cat > "$local_enterobase_folder/restart_local_enterobase.sh" <<EOF
#!/bin/bash
script_folder=\$(dirname \$(readlink -f \$0))
sing_list=\`singularity instance list\`
#cehck if the instance is runing and if so it will stop it
if [[ \$sing_list =~ "local_enterobase" ]];
then
   echo "local_enterobase is running ..!"
   singularity instance stop local_enterobase
fi
#singularity instance  stop local_enterobase
singularity instance start \$script_folder/local_enterobase.sif local_enterobase
EOF
sudo chown $real_user:$real_user $local_enterobase_folder/restart_local_enterobase.sh
sudo chmod +x  $local_enterobase_folder/restart_local_enterobase.sh

#singularity instance start local_enterobase.sif local_enterobase
echo
echo
#Adding crontab jobs so he containter run when the system rebbot
#echo "$jobs" | crontab -u root -

#Nginx
nginx_cronjob="@reboot bash $nginx_folder/run_nginx.sh &"
root_cron_list=`sudo crontab -l`
echo
if [[ $root_cron_list =~ $nginx_cronjob ]]
then
   echo "nginx job is added before ..!"
else
  echo
  echo "Adding the nginx job ..."
 (crontab -u root -l; echo "$nginx_cronjob" ) | crontab -u root -
fi
echo
echo

#Postgres
postgres_cronjob="@reboot bash $postgres_folder/run_postgres.sh &"
user_cron_list=`sudo -u $real_user crontab -l`
echo
if [[ $user_cron_list =~ $postgres_cronjob ]]
then
   echo "postgres job is added before ..!"
else
  echo
  echo "Adding the postgres job ..."
 (crontab -u $real_user -l; echo "$postgres_cronjob" ) | crontab -u $real_user  -
fi
echo
echo

#Guncorn and applicationApplication
app_cronjob="@reboot bash $local_enterobase_folder/restart_local_enterobase.sh &"
#user_cron_list=`sudo -u $real_user crontab -l`
echo
if [[ $user_cron_list =~ $app_cronjob ]]
then
   echo "Application job is added before ..!"
else
  echo
  echo "Adding the application job ...."
 (crontab -u $real_user -l; echo "$app_cronjob" ) | crontab -u $real_user -
fi
echo
echo


#Create one script to runn all of the jobs, it is required sudo rights
echo
cat > "$local_enterobase_home/run_all.sh" <<EOF
#!/bin/bash
if ! [ \$(id -u) = 0 ]; then
   echo "The script need to be run as root." >&2
   exit 1
fi
sudo bash $nginx_folder/run_nginx.sh
bash $postgres_folder/run_postgres.sh
bash $local_enterobase_folder/restart_local_enterobase.sh
EOF

sudo chown $real_user:$real_user $local_enterobase_home/run_all.sh
sudo chmod +x  $local_enterobase_home/run_all.sh

#bash $local_enterobase_home/run_all.sh

sudo -u $real_user bash $local_enterobase_folder/restart_local_enterobase.sh

echo "Finished"
echo "open local EnteroBase configuration page with the system default browser, i.e. https://$server_address/update_system_configuration"



url_local=https://$server_address/update_system_configuration
if which xdg-open >/dev/null; then
sudo -u $real_user xdg-open $url_local & disown
fi
