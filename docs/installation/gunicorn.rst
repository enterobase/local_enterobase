Gunicorn and the application
----------------------------
* Please pull the singularity image file “local_enterobase.sif” from the singularity cloud library and save it inside your home folder, you should use this command:

  ::

    singularity pull --arch amd64  local_enterobase.sif library://local_enterobase/default/local_enterobase:0.1

* Please make sure that you have installed and run PostgreSQL container as described before.

* For security reason, you should first set up a system password so you can use the web interface to configure the application, register your client with Warwick EnteroBase and test upload files to Warwick EnteroBase. Use the following command to set up the password after replacing “mypassword” by your own password.

  ::

    singularity run --app set_password local_enterobase.sif -p mypassword

* Please note that you may get an error message regarding database configuration when running this command, but you can ignore it at this stage. Then, you should use the following command to the run the gunicron and the application:

  ::

    singularity instance start local_enterobase.sif local_enterobase

* To be able to use the application, you need to:

  * Configure the database server which includes Database server URI, Database port number, Database user and Database password.
  * Register your installation with Warwick EnteroBase
  * Test uploading 100 files to Warwick EnteroBase

* The local installation configuration file is saved in your home folder (.local_configuration_file.yml), you can edit it directly using any text editor (e.g. vim) or it can be alerted using “/update_system_configuration”  link from the web interface (it will be the default main web page if the database is not configured or not configured correctly).

* After any system configuration change, you need to restart the application using the following commands:

  ::

    singularity instance  stop local_enterobase
    singularity instance start local_enterobase.sif local_enterobase