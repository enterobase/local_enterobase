NGINX Image
-----------

Required files and folders
==========================

In "local_enterobase/Singularity_Images/NGINX" sub-folder

* Recipe file: **nginx.def**
* Default settings for nginx: **nginx_local_enterobase.conf**
* temporary ssl certificate: **"certs" folder**


Content of the recipe file: nginx.def
=====================================

* The first two lines in the def file instruct to pull the nginx docker image

::

  Bootstrap: docker
  From: nginx

* The following section copies the customized NGINX configuration file "nginx_local_enterobase.conf", as well as "certs" folder which contains temporary ssl certificate to a sub-folder inside the container. 

::

  %files
    local_enterobase/Singularity_Images/NGINX/nginx_local_enterobase.conf /var/www/
    local_enterobase/Singularity_Images/NGINX/certs/ /var/www/

* The following section creates an entry point which will be used to copy the configuration and temporary ssl certificate files to the host machine. It also creates a 'logs' folder. Please note that "/home/nginx_user/" a is virtual folder in the container which will be mapped at run time to a real folder in the machine which will host the container as it will be described in "Running NGINX Container" section.

::

  %apprun prep_nginx
   if ! test -f /home/nginx_user/nginx_local_enterobase.conf; then  cp /var/www/nginx_local_enterobase.conf /home/nginx_user/; fi
   if ! test -d /home/nginx_user/certs; then cp -R /var/www/certs /home/nginx_user; fi
   chmod 700 /home/nginx_user/certs
   if ! test -d /home/nginx_user/logs; then mkdir /home/nginx_user/logs; fi
   
* The following section creates another entry point, for validating the configuration file and checking the server status

::

  %apprun test_nginx
    nginx  -t -c /home/nginx_user/nginx_local_enterobase.conf


* Finally, the *startscript* section is used to start the server as a singularity instance

::

  %startscript
    nginx -c /home/nginx_user/nginx_local_enterobase.conf


Build NGINX Image
=================

Assuming that the local_enterobase repository is saved in the sub-folder "local_enterobase" from the current working folder, the following command is used to build the NGINX singularity image which its name is "nginx_container.sif":

::

    sudo singularity build nginx_container.sif local_enterobase/Singularity_Images/NGINX/nginx.def


Running NGINX container
=======================

* The user needs to create a sub-folder in his machine, the Nginx configuration file and certs folder will be copied to this sub-folder. It will be binded to the "/home/nginx_user" in the container.

::

  mkdir $HOME/nginx_folder

Then to copy the nginx configuration file and certs folder to the sub-folder ($HOME/nginx_folder), it is rquired to bind this sub-folder ($HOME/nginx_folder) to the "home/nginx" inside the conainer and run the container with prep_nginx:

::

  singularity run -B $HOME/nginx_folder/:/home/nginx_user --app prep_nginx nginx_container.sif


* Then the user needs to edit the configuration file to add their server name or IP address in three different places, by replacing (replaced_by_your_server_uri) string with your actual server uri, e.g. myserver.com.

::

  vi $HOME/nginx_folder/nginx_local_enterobase.conf
  
  
* If he user wants to get a fresh copy from the customized configuration file, he needs to delete the current configuration file, then run the singularity image with **--app prep_nginx** option which is illustrated before.

 ::
 
    rm $HOME/nginx_folder/nginx_local_enterobase.conf
    singularity run -B $HOME/nginx_folder/:/home/nginx_user --app prep_nginx nginx_container.sif

* Use the following command to validate the configuration of the server. Change the 'nginx_local_enterobase.conf' if there is any error message. 

::

   sudo singularity run -B $HOME/nginx_folder:/home/nginx_user --writable-tmpfs --app test_nginx  nginx_container.sif


* Use the following command to start the Nginx server as singularity instance:

::

    sudo singularity instance start -B $HOME/nginx_folder:/home/nginx_user --writable-tmpfs  nginx_container.sif nginx_sing nginx_sing

* The instance name is nginx_sing