EToKi Image
-----------

Required files and folders
==========================

In "local_enterobase/Singularity_Images/EToKi" sub-folder

* Recipe file for the base image: **EToKi_reci_base.def**
* Recipe file for the application image : **EToKi_reci.def**

Content of the recipe files
===========================

* This image is built in two stages:

  * The output of the first stage is the base image which contains the dependency packages
  * The second one is build based on the image which is created from the first stage then it clones the  EToki source code from Github in addition to the installation of the python packages inside the requirements file

  * The purpose for doing this is to reduce the build time as

    * The the first image building time is long and it is not changed frequently,
    * It is required to build the second stage image when the EToKi code is changed which happens often and the build time for this image is short.

The first image is built using **"EToKi_reci_base.def"** recipe file.

* The first two lines in the recipe file instruct that this image is based on singularity image for ubintu 18.04

::

  Bootstrap:library
  From: ubuntu:18.04

* Then at %post section of the recipe file,the main dependency packages are installed.

The recipe file of the second stage is **"EToKi_reci.def"**

* The first two lines in the recipe ile instruct that the image is built based on the local image (which is generated from the first stage)

::

   Bootstrap:localimage
   From : ET_Python.sif

* Then at %post section, it:

  * Clones the EToKi sourse code and save it inside the image
  * Installs all the python dependency packages which are saved in the EToKi requirements.txt file

* Creat an enetry point to copy configure.ini file to the user home folder

::

 %apprun cp_configure
   cp /code/EToKi/modules/configure.ini $HOME

* Another entry point is created to run EToKi with the different options as a Singularity instance.

::

  %runscript
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/code/EToKi/externals/SPAdes-3.13.0-Linux/bin
  APP_PATH="/code/EToKi/"
  PYTHONPATH=$APP_PATH:$PYTHONPATH
  SPDPATH=SPAdes-3.13.0-Linux/bin
  PYTHONPATH=$SPDPATH:$PYTHONPATH
  echo $PYTHONPATH
  cd /code/EToKi
  python3 /code/EToKi/EToKi.py "$@"


Building the EToKi images
=========================

* Assuming that the local_enterobase repository is saved in the sub-folder "local_enterobase" from the current working folder, run the following command to build the EToKi singularity image which its name is "ET_Python.sif":

::

  sudo singularity build ET_Python.sif local_enterobase/Singularity_Images/EToKi/EToKi_reci_base.def


The following command is used to build the second image:

::

   sudo singularity build EToKi.sif local_enterobase/Singularity_Images/EToKi/EToKi_reci.def

* You may get error messages while building the image, something like that:

 ::
  
   E: You don't have enough free space in /var/cache/apt/archives/.
   FATAL:   failed to execute %post proc: exit status 100
   FATAL:   While performing build: while running engine: while running /usr/local/libexec/singularity/bin/starter: exit status 255

* If you got the previous error messages you may not have enough disk space to create the singularity temporary files, using sandbox option (**--sandbox**) may solve this issue.

:: 
  
  sudo singularity build --sandbox EToKi.sif local_enterobase/Singularity_Images/EToKi/EToKi_reci.def
  
* The previous command will build into a folder instead of single file. This can be good for the development and testing.

* I have tried to use different disk to be used for singularity temporary files but did not help

* I have used virtual machine for building the images and have worked fine
  

Running the container
=====================

* The user needs to run the following command to copy the required configure data file (configure.ini) to the his home folder:

::

  singularity run --app cp_configure  EToKi.sif

* Then, the user  should download:

  * usearch software, it is needed to submit a free licence request, the user should receive an email which contains a download link
  * Kraken database, the user can download it using this link ftp://ftp.ccb.jhu.edu/pub/data/kraken2_dbs/minikraken2_v2_8GB_201904_UPDATE.tgz

The user should save both of them to the same folder e.g. $HOME/EToKi_externals  and run the following command to configure EToKi:

::

  singularity run -B $HOME/configure.ini:/code/EToKi/modules/configure.ini  -B $HOME/EToKi_externals:/EToKi/externals  EToKi.sif  configure  --link_krakenDB /EToKi/externals/minikraken2/ --usearch /EToKi/externals/usearch8.0.1623_i86linux32

EToKi is ready to run but at run time, the configure.ini and the folder which contains u-search and the kraken database must be bound to the ones at the host machine. This can be done by adding the following option:

::

  -B $HOME/configure.ini:/code/EToKi/modules/configure.ini  -B $HOME/EToKi_externals:/EToKi/externals

* Please note that I have tested the container with assembly options, although other EToKi's options should be visible but,

* This is the assembly command, assuming that the files are saved in $HOME/EToKi/examples, is:

::

  singularity run -B $HOME/configure.ini:/code/EToKi/modules/configure.ini  -B $HOME/EToKi_externals:/EToKi/externals EToKi.sif prepare --pe $HOME/EToKi/examples/S_R1.fastq.gz,$HOME/EToKi/examples/S_R2.fastq.gz -p $HOME/EToKi/examples/prep_out

