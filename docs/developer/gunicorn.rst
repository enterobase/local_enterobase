Gunicorn and Application Image
------------------------------

Required files and folders
==========================

In "local_enterobase/Singularity_Images/Gunicorn_App" sub-folder

* Recipe file for the base image: **local_enterobase_reci_base.def**
* Recipe file for the application image : **local_enterobase_reci.def**

Content of the recipe files
===========================

* This image is built in two stages:

  * The first stage is building the base image which contains the dependency packages
  * The second one is based on the image which is generated from the first stage and adding local EnteroBase application and the container entry points.


* The reason for using two stages is to reduce the total buld time

  * Building of the first image takes long time and it is not changed often.
  * While building of the second one takes much less time, and we need to re-build this iamge when changing the Local EnteroBase code which happens frequently.

The first image is built using **"local_enterobase_reci_base.def"** recipe file.

* The first two lines in the recipe files pull the a singularity image which is based on ubintu 18.04

::

  Bootstrap:library
  From: ubuntu:18.04

* Then, at the post section, it downloads and installs the different dependency packages.


The second image is built using **"local_enterobase_reci.def"** recipe file

* The first two lines instruct that the image is built based on a local image, then sets the local image name.

::

   Bootstrap:localimage
   From : local_base_image.sif

* Then, at the files section, it copies the local enterobase project code to the image.

::

  %files
	$HOME/local_enterobase/ /var/www/

* Then it creates an entry point to create a password for the local adminstrator

::

  %apprun set_password "@"
    python3 /var/www/local_enterobase/manage.py set_local_password "$@"

* This entry point takes one argument which is the password

* Another entry point is created to run Gunicorn as a singularity instance

::

  %startscript
     cd /var/www/local_enterobase/
     APP_PATH="/var/www/local_enterobase/"
     PYTHONPATH=$APP_PATH:$PYTHONPATH
     echo $PYTHONPATH
     gunicorn -b 0.0.0.0:8000  "local_entero:create_app('production')" --timeout 300 --name "local_entero" --log-file=$HOME/logs/gunilog.log --bind=unix:$HOME/sock

* This instructs that Gunicorn runs Local Enterobase, listens to port 8000 and sets time out to be 300 seconds.


Building the images
===================

* Assuming that the local_enterobase repository is saved in the sub-folder "local_enterobase" from the current working folder, run the following command to build a singularity image named as "local_base_image.sif", the local_base_image is built using the following command:

::

  sudo singularity build local_base_image.sif local_enterobase/Singularity_Images/Gunicorn_App/local_enterobase_reci_base.def

* The following command is used to build the second image:

    sudo singularity build  local_enterobase.sif local_enterobase/Singularity_Images/Gunicorn_App/local_enterobase_reci.def

Running the container
=====================

* The user needs to set up a system password so they can use the web interface to configure the application, registers local EnteroBase instance with Warwick EnteroBase and tests upload files to Warwick EnteroBase.

  * The local admin should use the following command to set up the password after replacing “mypassword” to their own passwords:

    ::

      singularity run --app set_password local_enterobase.sif -p mypassword

  * Please note that the user may get error messages regarding database configuration when running the previous command, but these messages can be ignored at this stage

* Then the user needs to use the following command to the run the gunicron and the application as a singularity instance which is called "local_enterobase" (the user may choose to use any other name).

::

  singularity instance start local_enterobase.sif local_enterobase


* To be sure that the local_enterobase instance is running, the local admin can use the following command to list all the running Singularity instances:

::

  singularity instance list

* The output will include the running instance name "i.e. local_enterobase", it will look something like this:

+------------------------+------------+----------+----------------------------------+
| INSTANCE NAME          | PID        | IP       |IMAGE                             |
+========================+============+==========+==================================+
| local_enterobase       | 23456      |          | /home/khaled/local_enterobase.sif|
+------------------------+------------+----------+----------------------------------+

* If the user wants to restart the system, they should stop the instance first, then run it again using the following two commands:

::

  singularity instance  stop local_enterobase
  singularity instance start local_enterobase.sif local_enterobase

