.. Local EnteroBase documentation master file, created by
   sphinx-quickstart on Tue May 19 14:03:49 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Local EnteroBase's documentation!
============================================

The main purpose of this version is to enable our partners (users) to assemble their read files locally and send the strain metadata along with the assembly files to Warwick EnteroBase.
The source code repository address is https://bitbucket.org/enterobase/local_enterobase/.



.. toctree::
   :maxdepth: 2
   :caption: Contents:


   installation/install
   configure/configure
   app_manual/app_manual
   developer/developer







Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
